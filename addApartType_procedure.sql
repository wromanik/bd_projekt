#dodaje typ mieszkania

drop procedure if exists addApartType;
DELIMITER //

create
   procedure addApartType(IN type varchar(127), IN class char(16),
                                                    IN description varchar(4095))
begin
    INSERT INTO apartmenttype(TypeName, Class, Description) values (type, class, description);
end //

delimiter ;
