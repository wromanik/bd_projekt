create
    procedure createReview(IN guestID int, IN reservation int, 
	IN title_rev varchar(512), IN descr varchar(8192), IN user_rate int)
begin

    declare days int;
    declare date_from date;
    declare date_to date;
    declare current_datetime date;
    declare apart int;
    declare prev_rate float;
    declare new_rate float;

    select dateFrom, dateTo, idApartment, Rate from guest
    join reservation r on r.idReservation = guest.Reservation__FK
    join apartment a on r.Apartment__FK = a.idApartment
    where (Reservation__FK = reservation) and (guest_UserID__FK = guestID)
    into date_from, date_to, apart, prev_rate;

    set days = datediff(date_to, date_from);

    set current_datetime = current_date;

    if (prev_rate is null )
        then set new_rate = user_rate;
     elseif (prev_rate >= 0)
        then set new_rate = ((prev_rate+user_rate)/2);
    end if;
   
    UPDATE apartment SET Rate = new_rate where idApartment=apart;
    INSERT INTO review(Title, Description, Rate, Date, Reservation__FK, 
	Apartment__FK, Guest__FK, howLong_days) 
	values (title_rev, descr, user_rate, 
	current_datetime, reservation, apart, guestID, days);
end //

DELIMITER ;


