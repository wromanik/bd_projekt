drop function if exists canUserDoIt;
DELIMITER //

create
    function canUserDoIt(user_id int) returns binary(1)
begin

    declare permission_temp binary(1);
    declare ban binary(1);
    declare active binary(1);
    declare done int;
    declare permission binary(1) default 1;

   declare read_user cursor for select isActive, isBanned from user where idUser = user_id;

   declare continue handler for not found set done = 1;

    OPEN read_user;

    calc_perm: loop
        fetch read_user into active, ban;

        if done then
            leave calc_perm;
        end if;

        if ((ban = 0) and (active =1))
            then set permission_temp = 1;
        else
            set permission_temp = 0;
        end if;
        
        set permission = ((permission_temp) and (permission));
        
    end loop calc_perm;

    ClOSE read_user;
    
    return permission;

end //

DELIMITER;