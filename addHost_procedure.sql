#dodawanie hosta do mieszkania poza dodaniem mieszkania,
# przydatne przy dodawaniu wspolnika

drop procedure if exists addHost;
DELIMITER //


create
   procedure addHost(IN id_host int)
begin
    insert into host(host_UserID__FK, idApartment__FK) 
	values (id_host, last_insert_id());
end //

DELIMITER ;
