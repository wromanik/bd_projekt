drop function if exists calcPrice;
DELIMITER //

create
   function calcPrice(id_apart int, date_from date, date_to date) 
   returns float
begin

    declare one_night_price float;
    declare summary float;
    declare discount_val float;
    declare nights_value int;

    select oneNightPrice from apartment 
	where (idApartment = id_apart) into one_night_price;
    select Discount from apartment 
	where (idApartment = id_apart) into discount_val;

    set nights_value = datediff(date_to, date_from);

    if (discount_val = 0)
        then set summary = nights_value * one_night_price;
    else
        set summary = (nights_value * one_night_price)*(1.00-discount_val);
    end if;

    return summary;
end //

DELIMITER ;