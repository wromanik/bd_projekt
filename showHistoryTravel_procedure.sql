drop procedure if exists showHistoryTravel;
DELIMITER //

create
    procedure showHistoryTravel(IN user_id int)
begin
    select user.Name, user.Surname, l.City, c.Name as Country, c.Continent from user 
        join guest g on user.idUser = g.guest_UserID__FK
        join reservation r on g.Reservation__FK = r.idReservation
        join apartment a on r.Apartment__FK = a.idApartment
        join localization l on a.ApartmentLocalization__FK = l.idLocalization
        join country c on l.idCountry_FK = c.idCountry

    where idUser = user_id;
end //

DELIMITER ;
