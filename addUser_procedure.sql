#dodaje uzytkownika

drop procedure if exists addUser;
DELIMITER //
create
    procedure addUser(
	IN name varchar(127), IN surname varchar(127), IN birthdate date,
    IN address varchar(255), IN postcode varchar(15), IN city varchar(64),
    IN primaryphone int, IN secondaryphone int, IN email varchar(255),
    IN password varchar(255), IN is_active binary(1),
    IN is_confirmed binary(1), IN is_banned binary(1),
     IN comments varchar(8192), IN payment_method varchar(255),
    IN ishost binary(1), IN isguest binary(1), IN localization int)
begin
    INSERT INTO user(Name, Surname, BirthDate, Address, PostCode, City, 
	PrimaryPhoneNumber, SecondaryPhoneNumber, Email, Password, 
	isActive, isConfirmed, isBanned, 
	Comments, idPaymentMethod, isHost, isGuest, UserLocalization_FK) 
	values
    (name, surname, birthdate, address, postcode, city, 
	primaryphonenumber, secondaryphonenumber, email, md5(password), 
	isactive, isconfirmed, isbanned, 
	comments, md5(idpaymentmethod), ishost, isguest, userlocalization_fk);
end //

DELIMITER ;