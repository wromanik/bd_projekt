drop procedure if exists showHostContactData;
DELIMITER //


create
  procedure showHostContactData(IN host_id int)
begin

    declare temp_phonenumber int;
    declare temp_prefix int;
	declare phonenumber char(24);
    declare prefix char(10);
    declare is_host binary(1);
    
    select PrimaryPhoneNumber, Prefix, isHost from user
    join localization l on UserLocalization_FK = l.idLocalization
    join country c on l.idCountry_FK = c.idCountry 
	where idUser = host_id
    into temp_phonenumber, temp_prefix, is_host;
    set phonenumber = cast(temp_phonenumber as CHAR(24));
    set prefix = cast(temp_phonenumber as CHAR(10));
    set phonenumber = concat('+',prefix,phonenumber);

    proces:begin 
        if (is_host = 0)
			then select 'Host does not exists!';
            then leave proces;
        else
            select user.Name, Surname, Address, user.City, PostCode, 
			phonenumber, Email, isActive, isBanned, l2.City 
			as CurrentLocalization, c2.Name as Country 
			from user
            join localization l2 on UserLocalization_FK = l2.idLocalization
            join country c2 on l2.idCountry_FK = c2.idCountry
            where idUser = host_id;
        end if;
    end proces;
end //

DELIMITER ;