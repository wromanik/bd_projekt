#dodaje apartment i jego gospodarza

drop procedure if exists addApartment;


delimiter //

create procedure addApartment(IN aparttype int, IN localization int, IN addrress varchar(255),
                                                    IN apartnumber int, IN title varchar(255), IN descr varchar(8192),
                                                    IN equip varchar(2047), IN area float, IN rooms tinyint,
                                                    IN is_longterm binary(1), IN is_availaible binary(1),
                                                    IN one_night_price float, IN discount float,
                                                    IN comments varchar(4096), in id_host int)
begin


    INSERT INTO apartment(ApartmentType__FK, ApartmentLocalization__FK, Address, ApartNumber, Title, Description, Equipment, Area, Rooms, isLongTerm, isAvailable, oneNightPrice, Discount, Comments)
    values (aparttype, localization, addrress, apartnumber, title, descr, equip, area, rooms, is_longterm, is_availaible, one_night_price, discount, comments);

    INSERT INTO host(host_UserID__FK, idApartment__FK) values (id_host,last_insert_id());

end //
delimiter ;