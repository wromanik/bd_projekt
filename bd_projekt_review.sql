insert into review (idReview, Title, Description, Rate, Date, Reservation__FK, Apartment__FK, Guest__FK, howLong_days)
values  (21, 'Wszystko ok', 'Wszystko ok', 5, '2020-12-14', 1, 133, 528, 4),
        (22, 'Git', 'Git', 4, '2019-12-14', 7, 78, 641, 20),
        (23, 'Brud', 'Brud w łazience', 3, '2019-08-14', 7, 78, 601, 14),
        (24, 'OK', 'Dla mnie spoko', 5, '2020-08-19', 7, 78, 597, 41),
        (26, 'Złodzieje!', 'Nie mam portfela po pobycie tutajh', 1, '2020-12-19', 12, 134, 13, 24),
        (27, 'Super', 'Nic nie ginie', 4, '2020-12-14', 16, 134, 540, 3),
        (28, 'Ekstra', 'Uczciwi gospodarze', 4, '2020-12-16', 16, 134, 603, 5),
        (29, 'Dobrze', 'Odpoczalem', 5, '2020-12-29', 16, 134, 8, 4),
        (30, 'Dobrze', 'Jak wyzej', 4, '2020-11-14', 20, 148, 600, 6),
        (31, 'OK', 'Dobrze', 5, '2020-11-18', 20, 148, 604, 7),
        (32, 'OK', 'Troche za twarde materace', 4, '2020-11-14', 20, 148, 625, 10),
        (33, 'Wszystko dobrze', 'Ja nie narzekam', 5, '2020-12-14', 20, 148, 647, 24);
   