drop procedure if exists makeReservation;
DELIMITER //
create
     procedure makeReservation(IN user_id int, IN apart int, IN howmanyguest int,
                                                       IN datefrom datetime, IN dateto datetime,
                                                       IN comment varchar(4096))
begin
    declare isAllowed binary(1);
    declare isHostAllowed binary(1);
   
    declare price float;
    declare permission binary(1);

    select canUserDoIt(user_id) into isAllowed;
    select isHostActionsAllowed(apart) into isHostAllowed;


        if ((isHostAllowed = 1) and (isAllowed = 1))
        then set permission = 1;
        else
            set permission = 0;
    end if;

    process:begin
        if (permission = 0)
        then select 'Permission denied!';
        leave process;
        else

            select calcPrice(apart, datefrom, dateto) into price;
       
            INSERT INTO reservation(apartment__fk, howmanyguests, datefrom, dateto, price, comments) 
			VALUES (apart, howmanyguest, datefrom, dateto, price, comment);
            insert into guest(guest_UserID__FK, Reservation__FK) values (user_id, last_insert_id());
        end if;
        end process;
end //

DELIMITER ;

