#funkcja sumujaca dochody gospodarza (host) z wszystkich rezerwacji jego mieszkan

drop function if exists hostSumOfReservation;

delimiter //

create function hostSumOfReservation (id_host int)
	returns float

begin

    declare summary float default 0.00;
    select SUM(Price) from host
    join apartment a on host.idApartment__FK = a.idApartment
    join reservation r on a.idApartment = r.Apartment__FK
    where host_UserID__FK = id_host into summary;


    return summary;
end // 

delimiter ;