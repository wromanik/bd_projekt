drop procedure if exists showSumHost;
DELIMITER //

create
    procedure showSumHost(IN host int)
begin
    select Name, Surname, hostSumOfReservation(host)
    from user
    where idUser = host;
end //

DELIMITER ;