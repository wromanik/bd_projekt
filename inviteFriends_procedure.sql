drop procedure if exists inviteFriends;
DELIMITER //

create
    procedure inviteFriends(IN reservation_id int, IN id_guest int)
begin

    declare isAllowed binary(1);
    select canUserDoIt(id_guest) into isAllowed;

    proc:begin
        if (isAllowed = 0)
            then leave proc;
        else
            insert into guest (guest_UserID__FK, Reservation__FK) 
			values (id_guest, reservation_id);
        end if;
    end proc;
end //

DELIMITER ;