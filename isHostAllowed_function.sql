drop function if exists isHostActionsAllowed;
DELIMITER //

create
     function isHostActionsAllowed(apart_id int) returns binary(1)
begin

    declare permission_temp binary(1);
    declare ban binary(1);
    declare active binary(1);
    declare done int;
    declare permission binary(1) default 1;

    declare read_hosts cursor for select isActive, isBanned from apartment
    join host h2 on apartment.idApartment = h2.idApartment__FK
    join user u2 on h2.host_UserID__FK = u2.idUser where idApartment = apart_id;

   declare continue handler for not found set done = 1;

    OPEN read_hosts;

    calc_perm: loop
        fetch read_hosts into active, ban;

        if done then
            leave calc_perm;
        end if;

        if ((ban = 0) and (active =1))
            then set permission_temp = 1;
        else
            set permission_temp = 0;
        end if;
        
        set permission = ((permission_temp) and (permission));
        
    end loop calc_perm;

    ClOSE read_hosts;
    
    return permission;
end //

DELIMITER ;