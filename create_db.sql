#tylko skopiować i wkleić do istniejącej pustej bazy danych - sprawdzone pod kątem poprawności

create table IF NOT EXISTS apartmenttype
(
    idApartType int auto_increment,
    TypeName    varchar(127) charset utf8mb4  not null,
    Class       char(16) charset utf8mb4      not null,
    Description varchar(4095) charset utf8mb4 null,
    constraint ApartmentType_idApartType_uindex
        unique (idApartType)
)
    comment 'Klasy, rodzaje mieszkań';

alter table apartmenttype
    add primary key (idApartType);

create table IF NOT EXISTS country
(
    idCountry tinyint auto_increment,
    Name      varchar(128) not null,
    Code      varchar(8)   not null,
    Prefix    int          not null,
    Continent varchar(128) not null,
    constraint Country_idCountry_uindex
        unique (idCountry)
)
    comment 'Panstwa' charset = utf8mb4;

alter table country
    add primary key (idCountry);

create table IF NOT EXISTS localization
(
    idLocalization int auto_increment,
    City           varchar(255) charset utf8mb4 not null,
    idCountry_FK   tinyint                      not null,
    constraint localization_idLocalization_uindex
        unique (idLocalization),
    constraint country___fk
        foreign key (idCountry_FK) references country (idCountry)
)
    comment 'Miasta, miejscowości';

alter table localization
    add primary key (idLocalization);



create table IF NOT EXISTS apartment
(
    idApartment               int auto_increment,
    ApartmentType__FK         int           not null,
    ApartmentLocalization__FK int           not null,
    Address                   varchar(255)  not null,
    ApartNumber               int           null,
    Title                     varchar(255)  not null,
    Description               varchar(8192) null,
    Equipment                 varchar(2047) not null,
    Area                      float         not null,
    Rooms                     tinyint       not null,
    isLongTerm                binary(1)     not null,
    isAvailable               binary(1)     not null,
    oneNightPrice             float         not null,
    Discount                  float         null,
    Comments                  varchar(4096) null,
    Rate                      float         null,
    constraint Apartment_idApartment_uindex
        unique (idApartment),
    constraint ApartmentLocalization__FK
        foreign key (ApartmentLocalization__FK) references localization (idLocalization),
    constraint ApartmentType__FK
        foreign key (ApartmentType__FK) references apartmenttype (idApartType)
)
    comment 'Apartamenty, pokoje, mieszkania, etc.' charset = utf8mb4;

alter table apartment
    add primary key (idApartment);
	
create table IF NOT EXISTS user
(
    idUser               int auto_increment,
    Name                 varchar(127)  not null,
    Surname              varchar(127)  not null,
    BirthDate            date          not null,
    Address              varchar(255)  not null,
    PostCode             varchar(15)   not null,
    City                 varchar(64)   not null,
    PrimaryPhoneNumber   bigint        not null,
    SecondaryPhoneNumber int           null,
    Email                varchar(255)  not null,
    Password             varchar(255)  not null,
    isActive             binary(1)     not null,
    isConfirmed          binary(1)     not null,
    isBanned             binary(1)     not null,
    Comments             varchar(8192) null,
    idPaymentMethod      varchar(255)  not null,
    isHost               binary(1)     not null,
    isGuest              binary(1)     not null,
    UserLocalization_FK  int           null,
    constraint User_idUser_uindex
        unique (idUser),
    constraint user_Email_uindex
        unique (Email),
    constraint user_idPaymentMethod_uindex
        unique (idPaymentMethod),
    constraint UserLocalization___fk
        foreign key (UserLocalization_FK) references localization (idLocalization)
            on update set null on delete set null
)
    charset = utf8mb4;

alter table user
    add primary key (idUser);
	
create table IF NOT EXISTS reservation
(
    idReservation    int auto_increment,
    Apartment__FK    int           not null,
    howManyGuests    int           not null,
    dateFrom         datetime      not null,
    dateTo           datetime      not null,
    Price            float         not null,
    Comments         varchar(4096) null,
    constraint Reservation_idReservation_uindex
        unique (idReservation),
    constraint Apartment_Reservation___FK
        foreign key (Apartment__FK) references apartment (idApartment)
            on update cascade on delete cascade
)
    comment 'Rezerwacje' charset = utf8mb4;

alter table reservation
    add primary key (idReservation);

create table IF NOT EXISTS host
(
    host_UserID__FK int not null,
    idApartment__FK int not null,
    constraint Apartment___fk
        foreign key (idApartment__FK) references apartment (idApartment)
            on update cascade on delete cascade,
    constraint host_user_idUser__FK
        foreign key (host_UserID__FK) references user (idUser)
            on update cascade on delete cascade
)
    charset = utf8mb4;

create table IF NOT EXISTS guest
(
    guest_UserID__FK int not null,
    Reservation__FK  int null,
    constraint Guest__FK
        foreign key (guest_UserID__FK) references user (idUser)
            on update cascade on delete cascade,
    constraint Reservation___FK
        foreign key (Reservation__FK) references reservation (idReservation)
)
    charset = utf8mb4;

create table review
(
    idReview        int auto_increment,
    Title           varchar(512)  not null,
    Description     varchar(8192) not null,
    Rate            tinyint       not null,
    Date            date          not null,
    Reservation__FK int           not null,
    Apartment__FK   int           not null,
    Guest__FK       int           not null,
    howLong_days    int           not null,
    constraint review_idReview_uindex
        unique (idReview),
    constraint Apartment__FK
        foreign key (Apartment__FK) references apartment (idApartment)
            on update cascade on delete cascade,
    constraint Guest___FK
        foreign key (Guest__FK) references guest (guest_UserID__FK)
            on update cascade on delete cascade,
    constraint ReservationReview__FK
        foreign key (Reservation__FK) references guest (Reservation__FK)
            on update cascade on delete cascade
)
    comment 'recenzje' charset = utf8mb4;

alter table review
    add primary key (idReview);

insert into apartmenttype (idApartType, TypeName, Class, Description)
values  (1, 'AparmentLux', 'A+', 'Opcja luksusowa - konieczna certyfikacja i rygorystyczne kontrole jakosci '),
        (2, 'Samodzielny apartament', 'A', 'Najwyzszy standard potwierdzony certyfikatem organizacji'),
        (3, 'Samodzielny apartament', 'B', 'Powyzej przecietnej'),
        (4, 'Samodzielny apartament', 'C', 'Samodzielny apartament zapewniajacy podstawowe funkcje'),
        (5, 'Samodzielny pokoj z wspoldzielona lazienka', 'A', 'pokoj zapewniajacy najwyzszy komfort, rekomendacja organizacji'),
        (6, 'Samodzielny pokoj z wspoldzielona lazienka', 'B', 'pokoj komfortowy'),
        (7, 'Samodzielny pokoj z wspoldzielona lazienka', 'C', 'Standard turystyczny'),
        (8, 'pokoj hotelowy', 'A', '5 gwiazdek'),
        (9, 'pokoj hotelowy', 'B', '4 gwiazdki'),
        (10, 'pokoj hotelowy', 'C', '3 gwiazdki'),
        (11, 'pokoj hotelowy', 'D', '2 gwiazdki'),
        (12, 'pokoj hotelowy', 'E', '1 gwiazkda'),
        (13, 'pokoj hotelowy', 'F', 'Standard turystyczny'),
        (14, 'Miejsce w noclegowni turystycznej', '--NK--', 'Najtansza opcja dla obiezyswiatow, przede wszystkim dla lubiacych ludzi ');

insert into country (idCountry, Name, Code, Prefix, Continent)
values  (1, 'Japonia', 'JPN', 81, 'Azja'),
        (2, 'Korea Południowa', 'KOR', 82, 'Azja'),
        (3, 'Izrael', 'ISR', 972, 'Azja'),
        (4, 'Egipt', 'EGY', 20, 'Afryka'),
        (5, 'Maroko', 'MAR', 212, 'Afryka'),
        (6, 'Madagaskar', 'MDG', 261, 'Afryka'),
        (7, 'Australia', 'AUS', 61, 'Australia i Oceania'),
        (8, 'Nowa Zelandia', 'NZL', 64, 'Australia i Oceania'),
        (9, 'Polska', 'POL', 48, 'Europa'),
        (10, 'Islandia', 'ISL', 354, 'Europa'),
        (11, 'Chorwacja', 'HRV', 385, 'Europa'),
        (12, 'Stany Zjednoczone', 'USA', 1, 'Ameryka Północna'),
        (13, 'Kanada', 'CAN', 1, 'Ameryka Północna'),
        (14, 'Meksyk', 'MEX', 52, 'Ameryka Północna'),
        (15, 'Chile', 'CHL', 56, 'Ameryka Południowa'),
        (16, 'Argentyna', 'ARG', 54, 'Ameryka Południowa'),
        (17, 'Brazylia', 'BRA', 55, 'Ameryka Południowa'),
        (18, 'Rosja', 'RUS', 7, 'Europa / Azja');
		
insert into localization (idLocalization, City, idCountry_FK)
values  (1, 'Sapporo', 1),
        (2, 'Tokyo', 1),
        (3, 'Hokkaido', 1),
        (4, 'Seul', 2),
        (5, 'Pusan', 2),
        (6, 'Jeju', 2),
        (7, 'Jerozolima', 3),
        (8, 'Betlejem', 3),
        (9, 'Tel Awiw', 3),
        (10, 'Aleksandria', 4),
        (11, 'Kair', 4),
        (12, 'Luksor', 4),
        (13, 'Marrakesz', 5),
        (14, 'Fez', 5),
        (15, 'Casablanca', 5),
        (16, 'Antananarywa', 6),
        (17, 'Antsirabe', 6),
        (18, 'Toamasina', 6),
        (19, 'Sydney', 7),
        (20, 'Melbourne', 7),
        (21, 'Perth', 7),
        (22, 'Wellington', 8),
        (23, 'Christchurch', 8),
        (24, 'Auckland', 8),
        (25, 'Warszawa', 9),
        (26, 'Kraków', 9),
        (27, 'Radom', 9),
        (28, 'Rejkiawik', 10),
        (29, 'Isafjordur', 10),
        (30, 'Holmavik', 10),
        (31, 'Zagrzeb', 11),
        (32, 'Dubrownik', 11),
        (33, 'Zadar', 11),
        (34, 'New York', 12),
        (35, 'Las Vegas', 12),
        (36, 'Albuquerque', 12),
        (37, 'Toronto', 13),
        (38, 'Montreal', 13),
        (39, 'Vancouver', 13),
        (40, 'Miasto Meksyk', 14),
        (41, 'Merida', 14),
        (42, 'Cuetzalan', 14),
        (43, 'Santiago', 15),
        (44, 'Vina del Mar', 15),
        (45, 'Iquique', 15),
        (46, 'Cordoba', 16),
        (47, 'Comodoro Rivadavia', 16),
        (48, 'General Roca', 16),
        (49, 'Brazilia', 17),
        (50, 'Sao Paolo', 17),
        (51, 'Fortaleza', 17),
        (52, 'Moskwa', 18),
        (53, 'Sant Petersburg', 18),
        (54, 'Jakuck', 18);
		
insert into apartment (idApartment, ApartmentType__FK, ApartmentLocalization__FK, Address, ApartNumber, Title, Description, Equipment, Area, Rooms, isLongTerm, isAvailable, oneNightPrice, Discount, Comments, Rate)
values  (1, 12, 48, '22288 New Castle Crossing', 1, 'engineer interactive initiatives', 'Devolved maximized circuit', 'wieszaki,suszarka,kosmetyki', 132.56, 2, 0x30, 0x30, 83.92, 0.66, '', 3.8),
        (2, 3, 38, '59045 Sunnyside Center', 5, 'revolutionize web-enabled ROI', 'Diverse motivating frame', 'klimatyzacja, odswiezacz powietrza', 70.84, 1, 0x31, 0x30, 730.43, 0.41, null, 0.32),
        (3, 14, 11, '87 Rockefeller Way', 3, 'extend user-centric deliverables', 'Cross-group real-time task-force', 'pralka,mikrofalowka,rolety', 110.95, 1, 0x30, 0x31, 331.66, 0.12, null, 4.87),
        (4, 6, 24, '06158 Sunbrook Road', 3, 'visualize sexy e-commerce', 'Horizontal heuristic monitoring', 'zelazko,lozko wodne, dmuchany materac', 45.04, 3, 0x31, 0x30, 783.21, 0.63, null, 4.83),
        (5, 13, 21, '97677 Everett Lane', 5, 'integrate integrated functionalities', 'Up-sized incremental standardization', 'jacuzzi, waga, zastawa stolowa', 68.82, 5, 0x30, 0x31, 967.82, 0.14, 'Mandatory high-level support', 0.26),
        (6, 7, 3, '5 Eastwood Court', 1, 'morph global infomediaries', 'Secured fault-tolerant installation', 'zelazko,lozko wodne, dmuchany materac', 44.05, 2, 0x30, 0x30, 108.52, 0.64, null, null),
        (7, 2, 46, '495 Maple Wood Parkway', 1, 'transform transparent methodologies', 'Implemented exuding framework', 'zelazko,lozko wodne, dmuchany materac', 67.44, 5, 0x30, 0x30, 593.23, 0.76, null, null),
        (8, 2, 18, '10682 Calypso Point', 2, 'extend distributed content', 'Profound contextually-based function', 'wieszaki,suszarka,kosmetyki', 58.74, 2, 0x30, 0x30, 114.55, 0.26, null, null),
        (9, 1, 8, '00 Hoard Crossing', 5, 'embrace innovative content', 'Front-line client-server middleware', 'wieszaki,suszarka,kosmetyki', 32.42, 2, 0x31, 0x30, 988.36, 0.51, 'Multi-tiered fault-tolerant open architecture', 3.35),
        (10, 11, 1, '2 Stuart Drive', 1, 'empower vertical action-items', 'Enterprise-wide fresh-thinking Graphic Interface', 'pralka,mikrofalowka,rolety', 113.92, 1, 0x31, 0x31, 264.52, 0.49, 'Exclusive grid-enabled standardization', null),
        (11, 14, 27, '1 Artisan Pass', 4, 'cultivate next-generation web services', 'User-friendly explicit frame', 'pralka,mikrofalowka,rolety', 76.84, 3, 0x30, 0x30, 310.52, 0.29, null, 1.78),
        (12, 13, 40, '19 Hermina Parkway', 3, 'synergize virtual markets', 'Synchronised 4th generation open system', 'zelazko,lozko wodne, dmuchany materac', 89.33, 2, 0x31, 0x31, 464.94, 0.59, null, 4.22),
        (13, 1, 22, '07 Mockingbird Alley', 4, 'streamline proactive content', 'Visionary real-time application', 'jacuzzi, waga, zastawa stolowa', 18.35, 5, 0x31, 0x30, 398.5, 0.15, 'Universal solution-oriented implementation', 1.27),
        (14, 4, 3, '02 Jenna Plaza', 4, 'utilize back-end technologies', 'Implemented human-resource archive', 'pralka,mikrofalowka,rolety', 39.78, 3, 0x31, 0x31, 990.39, 0.38, null, 4.52),
        (15, 12, 13, '69 Toban Park', 5, 'leverage sticky supply-chains', 'Inverse impactful array', 'fotel z masazem, tv, radio', 34.21, 4, 0x30, 0x31, 387.25, 0.39, null, 3.57),
        (16, 12, 26, '89891 Cordelia Pass', 3, 'exploit dot-com portals', 'Profit-focused leading edge emulation', 'projektor,internet,biurko', 39.47, 3, 0x30, 0x30, 817.21, 0.65, null, 4.26),
        (17, 1, 19, '53 Katie Trail', 2, 'exploit innovative methodologies', 'Managed hybrid infrastructure', 'wazon, przypory kosmetyczne, swiatlowod', 29.64, 2, 0x30, 0x31, 883.97, 0.61, null, 2.63),
        (18, 10, 13, '43668 Havey Drive', 1, 'harness enterprise metrics', 'Managed full-range open architecture', 'klimatyzacja, odswiezacz powietrza', 57.94, 4, 0x31, 0x30, 320.46, 0.25, null, 2.52),
        (19, 8, 24, '8757 Del Sol Terrace', 5, 'morph impactful content', 'Multi-channelled value-added intranet', 'zelazko,lozko wodne, dmuchany materac', 66.15, 4, 0x30, 0x30, 112.06, 0.14, null, 0.27),
        (20, 3, 5, '7 Oak Place', 3, 'envisioneer ubiquitous metrics', 'Synergistic cohesive standardization', 'prasa, kawa, minibarek', 133.65, 2, 0x31, 0x31, 398.73, 0.36, null, 3.38),
        (21, 11, 50, '6016 Monterey Pass', 4, 'optimize customized portals', 'Re-contextualized context-sensitive protocol', 'fotel z masazem, tv, radio', 22.7, 4, 0x31, 0x30, 473.62, 0.81, 'Cross-platform eco-centric initiative', null),
        (22, 5, 20, '28850 Crowley Center', 4, 'facilitate sexy supply-chains', 'Intuitive holistic instruction set', 'fotel z masazem, tv, radio', 44.84, 1, 0x30, 0x31, 800.45, 0.8, null, 1.58),
        (23, 9, 21, '95 Hovde Crossing', 3, 'engage bricks-and-clicks web-readiness', 'Right-sized zero defect forecast', 'jacuzzi, waga, zastawa stolowa', 108.95, 5, 0x30, 0x30, 229.13, 0.23, null, 0.96),
        (24, 2, 15, '61 Corry Junction', 1, 'aggregate out-of-the-box e-markets', 'Digitized logistical strategy', 'wazon, przypory kosmetyczne, swiatlowod', 148.63, 4, 0x30, 0x30, 819.86, 0.15, null, 0.13),
        (25, 13, 44, '84220 Graedel Park', 4, 'exploit turn-key systems', 'Inverse logistical initiative', 'prasa, kawa, minibarek', 93.31, 4, 0x30, 0x31, 710.06, 0.26, null, 0.05),
        (26, 8, 5, '76799 Anthes Trail', 2, 'engage magnetic action-items', 'Assimilated maximized archive', 'wazon, przypory kosmetyczne, swiatlowod', 78.13, 2, 0x31, 0x30, 253.22, 0.18, 'Quality-focused high-level moderator', 4.64),
        (27, 2, 22, '90110 Bobwhite Crossing', 1, 'integrate collaborative systems', 'Integrated fault-tolerant infrastructure', 'wazon, przypory kosmetyczne, swiatlowod', 66.47, 3, 0x30, 0x31, 224.27, 0.05, null, 1.75),
        (28, 5, 10, '7505 Stang Alley', 2, 'e-enable impactful schemas', 'Advanced multi-state intranet', 'fotel z masazem, tv, radio', 62.49, 4, 0x31, 0x30, 342.29, 0.38, 'Seamless user-facing algorithm', 2.93),
        (29, 1, 32, '241 Ryan Lane', 1, 'leverage best-of-breed web services', 'Intuitive methodical paradigm', 'wazon, przypory kosmetyczne, swiatlowod', 147.95, 2, 0x31, 0x30, 968.65, 0.26, null, 0.19),
        (30, 1, 13, '591 Mayer Center', 4, 'morph turn-key markets', 'Reduced solution-oriented open architecture', 'kieliszki do szampana,jacuzzi,hifi', 129.9, 3, 0x30, 0x31, 169.11, 0.26, null, 2.5),
        (31, 10, 50, '340 Armistice Park', 2, 'mesh turn-key ROI', 'Synchronised homogeneous protocol', 'wieszaki,suszarka,kosmetyki', 97.18, 4, 0x30, 0x30, 454.36, 0.63, null, 1.55),
        (32, 7, 43, '28448 Havey Center', 3, 'deploy back-end deliverables', 'Cloned 4th generation middleware', 'kieliszki do szampana,jacuzzi,hifi', 7.38, 1, 0x30, 0x31, 557.63, 0.8, null, 4.95),
        (33, 12, 2, '879 Heffernan Street', 5, 'facilitate scalable ROI', 'Multi-layered local solution', 'projektor,internet,biurko', 68.14, 2, 0x30, 0x30, 98.78, 0.59, null, 4.23),
        (34, 11, 37, '6 Sachtjen Alley', 5, 'unleash cutting-edge functionalities', 'Synchronised systemic knowledge base', 'zelazko,lozko wodne, dmuchany materac', 74.31, 1, 0x31, 0x31, 212.73, 0.07, 'Multi-layered 5th generation structure', 4.13),
        (35, 10, 40, '229 Springview Plaza', 3, 'implement value-added markets', 'Horizontal multi-tasking adapter', 'jacuzzi, waga, zastawa stolowa', 5.12, 2, 0x31, 0x31, 312.87, 0.27, null, 1.03),
        (36, 12, 25, '1 Express Junction', 2, 'architect compelling schemas', 'Multi-lateral well-modulated functionalities', 'zelazko,lozko wodne, dmuchany materac', 28.77, 4, 0x30, 0x30, 246.1, 0.73, null, null),
        (37, 14, 38, '2 2nd Park', 5, 'optimize open-source e-tailers', 'Persevering 5th generation matrix', 'kieliszki do szampana,jacuzzi,hifi', 59.17, 1, 0x30, 0x31, 779.79, 0.02, 'Digitized bandwidth-monitored array', 2.33),
        (38, 14, 10, '60 Green Ridge Terrace', 4, 'streamline seamless schemas', 'Future-proofed secondary help-desk', 'pralka,mikrofalowka,rolety', 116.25, 1, 0x31, 0x31, 119.09, 0.19, null, 4.68),
        (39, 4, 37, '29 Anzinger Lane', 1, 'deploy extensible channels', 'Realigned hybrid customer loyalty', 'wazon, przypory kosmetyczne, swiatlowod', 76.01, 3, 0x30, 0x30, 557.03, 0.67, null, null),
        (40, 9, 51, '33617 Sullivan Way', 5, 'target innovative eyeballs', 'Implemented dynamic capacity', 'pralka,mikrofalowka,rolety', 26.43, 5, 0x30, 0x30, 76.38, 0.74, null, null),
        (41, 10, 24, '9 Cherokee Alley', 5, 'target killer markets', 'Intuitive context-sensitive knowledge base', 'pralka,mikrofalowka,rolety', 72.62, 5, 0x31, 0x31, 96.39, 0.33, null, 3.06),
        (42, 14, 5, '38 Homewood Parkway', 2, 'expedite ubiquitous relationships', 'Up-sized tangible product', 'projektor,internet,biurko', 128.2, 2, 0x30, 0x31, 621.63, 0.72, null, 0.99),
        (43, 5, 20, '21 Magdeline Park', 3, 'aggregate global methodologies', 'Secured value-added protocol', 'projektor,internet,biurko', 2.84, 5, 0x31, 0x31, 711.76, 0.64, 'Intuitive zero defect throughput', 1.6),
        (44, 9, 5, '027 Sutherland Avenue', 5, 'maximize holistic web-readiness', 'Universal radical hierarchy', 'klimatyzacja, odswiezacz powietrza', 9.47, 4, 0x30, 0x31, 440.43, 0.52, 'Enterprise-wide high-level database', 3.47),
        (45, 3, 2, '491 Mallard Plaza', 2, 'mesh ubiquitous action-items', 'Synchronised encompassing customer loyalty', 'wazon, przypory kosmetyczne, swiatlowod', 47.72, 5, 0x31, 0x31, 470.25, 0.29, 'Programmable fresh-thinking capability', 0.13),
        (46, 7, 36, '81 High Crossing Hill', 1, 'engineer bleeding-edge convergence', 'Organic holistic challenge', 'pralka,mikrofalowka,rolety', 107.42, 1, 0x31, 0x30, 295.56, 0.71, null, 0.67),
        (47, 4, 45, '7360 Shoshone Avenue', 5, 'incubate next-generation functionalities', 'Synchronised explicit alliance', 'klimatyzacja, odswiezacz powietrza', 9.94, 2, 0x30, 0x30, 237.28, 0.46, 'Proactive value-added service-desk', 4.07),
        (48, 1, 36, '15 Melvin Trail', 4, 'morph integrated ROI', 'Virtual static circuit', 'klimatyzacja, odswiezacz powietrza', 128.07, 5, 0x31, 0x31, 78.58, 0.23, null, 4.96),
        (49, 4, 47, '9 Crownhardt Court', 1, 'incubate real-time content', 'Quality-focused bandwidth-monitored pricing structure', 'jacuzzi, waga, zastawa stolowa', 79.82, 1, 0x31, 0x31, 75.31, 0.48, 'Future-proofed 6th generation knowledge base', 0.53),
        (50, 11, 35, '81843 Jenifer Junction', 2, 'extend one-to-one e-tailers', 'Networked non-volatile knowledge base', 'klimatyzacja, odswiezacz powietrza', 122.99, 2, 0x31, 0x31, 244.85, 0.23, null, 2.9),
        (51, 9, 29, '366 Fuller Park', 5, 'leverage back-end ROI', 'Innovative zero administration instruction set', 'klimatyzacja, odswiezacz powietrza', 107.77, 2, 0x31, 0x31, 709.41, 0.16, null, 0.32),
        (52, 9, 30, '5395 Laurel Road', 2, 'repurpose holistic mindshare', 'Face to face 3rd generation parallelism', 'pralka,mikrofalowka,rolety', 22.9, 4, 0x31, 0x30, 175.19, 0.69, 'Secured bi-directional info-mediaries', 0.5),
        (53, 3, 30, '727 Parkside Center', 5, 'productize customized users', 'De-engineered 3rd generation model', 'projektor,internet,biurko', 55.56, 1, 0x30, 0x30, 622.62, 0.11, null, 1.33),
        (54, 12, 4, '596 Clove Way', 5, 'incubate integrated methodologies', 'Upgradable mission-critical encryption', 'klimatyzacja, odswiezacz powietrza', 80.22, 3, 0x30, 0x31, 354.68, 0.8, null, null),
        (55, 2, 17, '0258 Buhler Hill', 5, 'deploy customized portals', 'Programmable user-facing encryption', 'zelazko,lozko wodne, dmuchany materac', 13.98, 3, 0x30, 0x31, 956.77, 0.2, 'Exclusive object-oriented contingency', 2.99),
        (56, 10, 9, '07450 Lotheville Lane', 1, 'transition visionary vortals', 'Reverse-engineered contextually-based budgetary management', 'zelazko,lozko wodne, dmuchany materac', 9.25, 3, 0x31, 0x31, 710.45, 0.43, null, null),
        (57, 6, 43, '2075 Crescent Oaks Parkway', 3, 'engineer clicks-and-mortar convergence', 'Centralized even-keeled capability', 'projektor,internet,biurko', 136.79, 2, 0x30, 0x31, 535.47, 0.61, null, 0.77),
        (58, 6, 19, '882 Eagle Crest Road', 1, 'whiteboard clicks-and-mortar systems', 'Operative attitude-oriented approach', 'wazon, przypory kosmetyczne, swiatlowod', 112.16, 5, 0x31, 0x30, 748.82, 0.73, null, null),
        (59, 14, 5, '130 Northview Terrace', 3, 'morph visionary e-markets', 'Right-sized neutral policy', 'wieszaki,suszarka,kosmetyki', 59.83, 4, 0x31, 0x30, 284.61, 0.53, null, null),
        (60, 7, 27, '7 Bay Parkway', 4, 'extend vertical eyeballs', 'Balanced holistic product', 'fotel z masazem, tv, radio', 65.11, 5, 0x31, 0x30, 823.34, 0.35, null, 4.8),
        (61, 11, 41, '146 Blackbird Plaza', 4, 'implement bricks-and-clicks communities', 'Extended optimal architecture', 'kieliszki do szampana,jacuzzi,hifi', 4.56, 5, 0x31, 0x31, 237.47, 0.02, null, null),
        (62, 11, 12, '01271 Texas Plaza', 2, 'embrace cross-media interfaces', 'Customizable client-server interface', 'wieszaki,suszarka,kosmetyki', 30.81, 4, 0x30, 0x31, 965.25, 0.11, null, 2.23),
        (63, 3, 28, '732 Corry Parkway', 2, 'morph B2B mindshare', 'Decentralized coherent service-desk', 'jacuzzi, waga, zastawa stolowa', 74.79, 3, 0x31, 0x30, 154.78, 0.6, null, 1.36),
        (64, 5, 23, '66361 Mayfield Plaza', 5, 'synergize visionary channels', 'Advanced 3rd generation intranet', 'kieliszki do szampana,jacuzzi,hifi', 81.06, 2, 0x31, 0x30, 548.18, 0.59, null, 1.62),
        (65, 8, 18, '88 Butterfield Road', 3, 'strategize front-end e-commerce', 'Pre-emptive disintermediate architecture', 'prasa, kawa, minibarek', 27.27, 4, 0x31, 0x30, 837.76, 0.89, null, 0.32),
        (66, 1, 36, '5 Hooker Circle', 5, 'facilitate enterprise mindshare', 'Integrated zero administration pricing structure', 'wieszaki,suszarka,kosmetyki', 14.11, 3, 0x31, 0x31, 462.27, 0.27, null, 4.1),
        (67, 12, 2, '5 Atwood Park', 4, 'deliver rich web-readiness', 'Focused motivating access', 'jacuzzi, waga, zastawa stolowa', 80.33, 4, 0x30, 0x30, 482.68, 0.02, null, null),
        (68, 10, 52, '97 Laurel Street', 4, 'strategize collaborative partnerships', 'Distributed cohesive functionalities', 'fotel z masazem, tv, radio', 72.54, 4, 0x30, 0x30, 287.3, 0.57, null, 3.57),
        (69, 8, 19, '6312 Sundown Center', 5, 'mesh cross-media partnerships', 'Future-proofed real-time policy', 'wieszaki,suszarka,kosmetyki', 29.56, 1, 0x30, 0x30, 322.79, 0.26, null, 4.51),
        (70, 4, 22, '4 Erie Trail', 2, 'repurpose revolutionary functionalities', 'Integrated client-server capacity', 'prasa, kawa, minibarek', 123.83, 5, 0x31, 0x30, 445.53, 0.73, null, 0.01),
        (71, 5, 25, '4388 American Ash Alley', 5, 'scale revolutionary bandwidth', 'Secured responsive firmware', 'wazon, przypory kosmetyczne, swiatlowod', 122.06, 1, 0x31, 0x31, 824.89, 0.88, null, null),
        (72, 6, 47, '0 Jackson Parkway', 4, 'e-enable bricks-and-clicks applications', 'Synergistic local policy', 'klimatyzacja, odswiezacz powietrza', 146.91, 2, 0x30, 0x30, 294.7, 0, null, 4.41),
        (73, 8, 19, '10 Lukken Park', 3, 'optimize transparent infrastructures', 'Reduced needs-based orchestration', 'wazon, przypory kosmetyczne, swiatlowod', 99.47, 5, 0x31, 0x30, 833.58, 0.49, null, 2.14),
        (74, 14, 36, '246 2nd Way', 4, 'enhance granular deliverables', 'Grass-roots impactful monitoring', 'wieszaki,suszarka,kosmetyki', 46.66, 5, 0x30, 0x30, 712.68, 0.03, null, 3.3),
        (75, 10, 49, '91 Green Street', 2, 'incubate value-added ROI', 'Profit-focused dedicated moderator', 'projektor,internet,biurko', 114.32, 3, 0x30, 0x30, 410.16, 0.5, null, 4.4),
        (76, 13, 12, '7213 Bluejay Terrace', 4, 'implement front-end technologies', 'Cloned intermediate local area network', 'projektor,internet,biurko', 16.2, 4, 0x30, 0x30, 401.95, 0.44, 'Grass-roots explicit task-force', null),
        (77, 10, 3, '800 Thierer Alley', 5, 'syndicate open-source markets', 'Horizontal hybrid function', 'fotel z masazem, tv, radio', 55.31, 4, 0x30, 0x31, 541.33, 0.07, 'Pre-emptive logistical approach', 3.13),
        (78, 10, 8, '94 Waywood Lane', 5, 'strategize world-class portals', 'Automated fresh-thinking attitude', 'pralka,mikrofalowka,rolety', 128.12, 3, 0x31, 0x31, 197.06, 0.27, null, 2.91),
        (79, 12, 23, '041 7th Hill', 2, 'reintermediate holistic networks', 'Centralized even-keeled leverage', 'pralka,mikrofalowka,rolety', 42.45, 3, 0x30, 0x30, 390.53, 0.88, null, 3.49),
        (80, 3, 49, '85 Mccormick Plaza', 5, 'visualize out-of-the-box applications', 'Open-architected 5th generation installation', 'wazon, przypory kosmetyczne, swiatlowod', 10.97, 2, 0x30, 0x31, 711.58, 0.89, null, 2.96),
        (81, 13, 26, '4231 Debra Court', 1, 'enable compelling solutions', 'Phased 6th generation knowledge user', 'kieliszki do szampana,jacuzzi,hifi', 28.33, 3, 0x31, 0x30, 213.37, 0.04, 'Function-based client-server solution', 4.58),
        (82, 9, 34, '96930 Wayridge Pass', 2, 'reintermediate integrated ROI', 'Reverse-engineered regional utilisation', 'fotel z masazem, tv, radio', 46.83, 3, 0x31, 0x31, 695.92, 0.37, null, null),
        (83, 3, 10, '8459 Carioca Drive', 4, 'e-enable leading-edge models', 'Realigned contextually-based firmware', 'jacuzzi, waga, zastawa stolowa', 73.94, 5, 0x30, 0x30, 860.22, 0.78, null, 2.3),
        (84, 6, 3, '8 Eliot Plaza', 5, 'grow interactive web services', 'Vision-oriented contextually-based open architecture', 'pralka,mikrofalowka,rolety', 141.15, 4, 0x30, 0x30, 711.4, 0.84, 'Configurable discrete moderator', null),
        (85, 12, 24, '682 Bay Parkway', 3, 'aggregate e-business networks', 'Multi-tiered 5th generation analyzer', 'prasa, kawa, minibarek', 131.96, 2, 0x30, 0x31, 440.04, 0.02, null, 0.31),
        (86, 7, 9, '4572 Ruskin Avenue', 5, 'redefine cross-media channels', 'Synergistic stable open architecture', 'jacuzzi, waga, zastawa stolowa', 104.31, 2, 0x31, 0x30, 537.69, 0.86, null, null),
        (87, 10, 41, '5 Algoma Place', 4, 'revolutionize holistic architectures', 'Centralized motivating superstructure', 'pralka,mikrofalowka,rolety', 69.22, 1, 0x31, 0x31, 846.08, 0.26, null, 2.76),
        (88, 2, 19, '71 Buena Vista Court', 3, 'incentivize out-of-the-box mindshare', 'Total regional matrix', 'jacuzzi, waga, zastawa stolowa', 54.21, 1, 0x31, 0x30, 599.31, 0.1, null, null),
        (89, 4, 53, '3 John Wall Junction', 4, 'evolve back-end e-services', 'Polarised real-time implementation', 'klimatyzacja, odswiezacz powietrza', 111.47, 5, 0x31, 0x30, 349.89, 0.63, null, 4.44),
        (90, 6, 19, '3 Transport Parkway', 5, 'productize 24/365 e-business', 'Robust asymmetric budgetary management', 'pralka,mikrofalowka,rolety', 58.31, 1, 0x31, 0x30, 107.99, 0.35, null, 2.42),
        (91, 1, 31, '4 Barby Street', 4, 'grow turn-key schemas', 'Synergistic intermediate analyzer', 'klimatyzacja, odswiezacz powietrza', 5.44, 5, 0x30, 0x31, 634.04, 0.33, null, 1.8),
        (92, 3, 36, '80593 Graedel Hill', 5, 'enhance efficient initiatives', 'Future-proofed secondary solution', 'pralka,mikrofalowka,rolety', 68.81, 3, 0x31, 0x31, 119.89, 0.61, null, 3.88),
        (93, 1, 9, '9596 Myrtle Circle', 2, 'redefine frictionless convergence', 'Switchable next generation groupware', 'pralka,mikrofalowka,rolety', 75.79, 1, 0x31, 0x30, 620.06, 0.03, null, 4.54),
        (94, 8, 29, '56 Forest Dale Road', 5, 'drive revolutionary supply-chains', 'Programmable directional leverage', 'klimatyzacja, odswiezacz powietrza', 87.21, 4, 0x31, 0x31, 425.38, 0.59, null, 4.35),
        (95, 1, 28, '14 Waxwing Plaza', 5, 'deliver out-of-the-box ROI', 'Future-proofed full-range workforce', 'pralka,mikrofalowka,rolety', 69.06, 3, 0x30, 0x30, 249.38, 0.33, null, null),
        (96, 13, 54, '29410 Jenna Place', 1, 'engage cutting-edge synergies', 'User-centric multimedia process improvement', 'pralka,mikrofalowka,rolety', 41.33, 4, 0x30, 0x30, 752.92, 0.18, null, null),
        (97, 12, 43, '31 Stephen Pass', 2, 'enhance rich portals', 'Synergistic web-enabled capability', 'wazon, przypory kosmetyczne, swiatlowod', 50.46, 5, 0x30, 0x30, 556.95, 0.17, null, 2.63),
        (98, 12, 51, '58652 Grasskamp Point', 2, 'envisioneer bleeding-edge schemas', 'Function-based clear-thinking knowledge user', 'wieszaki,suszarka,kosmetyki', 18.16, 3, 0x31, 0x30, 871.85, 0.2, null, 4.26),
        (99, 14, 28, '8 Carioca Alley', 2, 'incentivize transparent eyeballs', 'Managed real-time moratorium', 'prasa, kawa, minibarek', 24.49, 2, 0x30, 0x31, 342.72, 0.48, null, 1.92),
        (100, 5, 4, '2 Moose Pass', 1, 'whiteboard interactive synergies', 'Exclusive bottom-line frame', 'prasa, kawa, minibarek', 65.33, 5, 0x30, 0x31, 285.09, 0.63, null, 0.93),
        (101, 14, 49, '901 Orin Drive', 4, 'facilitate transparent architectures', 'Multi-tiered heuristic customer loyalty', 'pralka,mikrofalowka,rolety', 82.1, 3, 0x31, 0x31, 995.73, 0.14, null, 0.85),
        (102, 3, 51, '7 Bonner Plaza', 3, 'seize B2B action-items', 'Focused full-range database', 'kieliszki do szampana,jacuzzi,hifi', 114.1, 2, 0x30, 0x30, 870.13, 0.09, null, null),
        (103, 6, 19, '9601 Northridge Terrace', 4, 'harness robust solutions', 'Adaptive neutral moderator', 'pralka,mikrofalowka,rolety', 83.72, 2, 0x30, 0x31, 529.54, 0.7, 'Open-source composite artificial intelligence', 4.12),
        (104, 11, 51, '47 Eagle Crest Pass', 5, 'unleash bricks-and-clicks infrastructures', 'Adaptive discrete archive', 'kieliszki do szampana,jacuzzi,hifi', 19.51, 1, 0x30, 0x31, 618.73, 0.3, null, null),
        (105, 13, 29, '81651 Old Gate Drive', 1, 'streamline rich e-business', 'Profit-focused stable implementation', 'klimatyzacja, odswiezacz powietrza', 22, 2, 0x31, 0x30, 164.13, 0.65, null, 1.56),
        (106, 11, 47, '95685 Summerview Point', 4, 'incubate visionary networks', 'Advanced responsive hierarchy', 'klimatyzacja, odswiezacz powietrza', 27.36, 1, 0x30, 0x30, 302.72, 0.18, null, 4.85),
        (107, 14, 44, '1919 Fairfield Parkway', 1, 'integrate frictionless channels', 'Operative actuating projection', 'pralka,mikrofalowka,rolety', 30.7, 5, 0x30, 0x30, 129.1, 0.74, 'Function-based intangible approach', 1.24),
        (108, 7, 50, '50566 Boyd Alley', 4, 'harness open-source action-items', 'Phased intangible projection', 'jacuzzi, waga, zastawa stolowa', 3.32, 1, 0x30, 0x31, 781.27, 0.29, 'Synchronised system-worthy methodology', 0.92),
        (109, 10, 26, '69 Namekagon Terrace', 3, 'enable best-of-breed partnerships', 'Reverse-engineered holistic application', 'kieliszki do szampana,jacuzzi,hifi', 132.5, 4, 0x30, 0x30, 157.93, 0, 'Exclusive disintermediate extranet', 2.28),
        (110, 13, 13, '7 Arizona Street', 4, 'recontextualize bleeding-edge applications', 'Stand-alone content-based hub', 'fotel z masazem, tv, radio', 34.18, 2, 0x31, 0x31, 186.54, 0.55, null, 4.86),
        (111, 11, 43, '49177 Farmco Trail', 3, 'recontextualize collaborative infrastructures', 'Organic user-facing Graphical User Interface', 'zelazko,lozko wodne, dmuchany materac', 53.93, 5, 0x31, 0x30, 277.9, 0.34, null, 0.48),
        (112, 13, 33, '6 Buena Vista Court', 4, 'maximize rich systems', 'Adaptive neutral moderator', 'jacuzzi, waga, zastawa stolowa', 131.7, 5, 0x31, 0x31, 299.07, 0.09, null, 4.38),
        (113, 9, 29, '97 Straubel Way', 5, 'deploy extensible infomediaries', 'Synergistic 3rd generation concept', 'prasa, kawa, minibarek', 4.35, 5, 0x30, 0x31, 767.52, 0.59, 'Profit-focused uniform solution', 2.12),
        (114, 8, 38, '6 Cascade Park', 2, 'evolve synergistic technologies', 'Realigned zero administration analyzer', 'zelazko,lozko wodne, dmuchany materac', 92.42, 1, 0x30, 0x31, 129.25, 0.69, 'Enhanced object-oriented emulation', 2.64),
        (115, 11, 34, '036 Montana Road', 4, 'mesh bricks-and-clicks architectures', 'Virtual full-range artificial intelligence', 'klimatyzacja, odswiezacz powietrza', 105.33, 1, 0x31, 0x30, 238.3, 0.8, null, 1.25),
        (116, 10, 13, '7 Maywood Hill', 5, 'revolutionize plug-and-play web-readiness', 'Monitored high-level local area network', 'wazon, przypory kosmetyczne, swiatlowod', 46.33, 3, 0x31, 0x31, 565.7, 0.34, 'Customizable impactful throughput', null),
        (117, 12, 51, '7 Marquette Park', 5, 'synergize leading-edge technologies', 'Triple-buffered zero administration database', 'jacuzzi, waga, zastawa stolowa', 42.23, 5, 0x30, 0x30, 661.88, 0.19, null, null),
        (118, 8, 40, '445 Scoville Plaza', 5, 'target best-of-breed architectures', 'Front-line analyzing synergy', 'pralka,mikrofalowka,rolety', 43.59, 4, 0x30, 0x30, 455.63, 0.64, null, null),
        (119, 3, 46, '288 Troy Way', 4, 'extend proactive experiences', 'Networked mobile hub', 'prasa, kawa, minibarek', 10.27, 5, 0x30, 0x30, 224.06, 0.22, 'Innovative needs-based knowledge base', null),
        (120, 13, 53, '0853 Sauthoff Lane', 3, 'reinvent clicks-and-mortar content', 'Switchable tertiary productivity', 'wieszaki,suszarka,kosmetyki', 133.84, 3, 0x31, 0x30, 301.81, 0.73, null, 4.87),
        (121, 12, 11, '98 Barby Alley', 3, 'optimize web-enabled web-readiness', 'Triple-buffered needs-based contingency', 'klimatyzacja, odswiezacz powietrza', 119.82, 5, 0x30, 0x31, 798.41, 0.83, 'Visionary value-added help-desk', null),
        (122, 6, 33, '65 Manufacturers Junction', 5, 'transition turn-key users', 'Intuitive user-facing info-mediaries', 'wazon, przypory kosmetyczne, swiatlowod', 67.67, 2, 0x31, 0x30, 558.36, 0.72, null, 0.03),
        (123, 6, 44, '3 Dexter Crossing', 5, 'monetize cross-platform communities', 'Reactive real-time collaboration', 'klimatyzacja, odswiezacz powietrza', 4.19, 1, 0x30, 0x30, 847.81, 0.01, 'Streamlined secondary knowledge base', 4.53),
        (124, 3, 43, '49 Beilfuss Crossing', 5, 'deploy sexy synergies', 'User-friendly global database', 'klimatyzacja, odswiezacz powietrza', 25.94, 1, 0x30, 0x31, 398.79, 0.23, null, null),
        (125, 14, 6, '29287 Victoria Lane', 3, 'leverage enterprise initiatives', 'Exclusive foreground application', 'klimatyzacja, odswiezacz powietrza', 147.66, 2, 0x31, 0x31, 224.04, 0.02, 'Programmable explicit orchestration', 0.82),
        (126, 14, 28, '9518 Texas Avenue', 1, 'morph sexy bandwidth', 'Managed tangible ability', 'jacuzzi, waga, zastawa stolowa', 72.46, 4, 0x31, 0x30, 291.68, 0.45, null, null),
        (127, 7, 40, '35578 Loomis Trail', 2, 'reinvent open-source metrics', 'Cross-platform empowering capacity', 'kieliszki do szampana,jacuzzi,hifi', 109.79, 1, 0x31, 0x31, 214.04, 0.11, null, 4.76),
        (128, 13, 30, '8 Haas Point', 3, 'maximize best-of-breed metrics', 'Stand-alone systemic emulation', 'prasa, kawa, minibarek', 84.57, 3, 0x31, 0x31, 688.12, 0.22, null, 0.65),
        (129, 10, 51, '9344 Pankratz Way', 4, 'generate intuitive partnerships', 'Future-proofed directional circuit', 'jacuzzi, waga, zastawa stolowa', 80.09, 4, 0x30, 0x31, 895.67, 0.59, null, 0.09),
        (130, 6, 18, '8 Anzinger Terrace', 1, 'orchestrate user-centric methodologies', 'Object-based real-time benchmark', 'kieliszki do szampana,jacuzzi,hifi', 38.46, 5, 0x31, 0x30, 183.53, 0.52, null, null),
        (131, 9, 34, '9 Scoville Drive', 4, 'scale enterprise schemas', 'Organic client-server array', 'wieszaki,suszarka,kosmetyki', 32.34, 4, 0x31, 0x31, 82.55, 0.18, 'Total tangible core', 3.22),
        (132, 3, 20, '0096 Village Junction', 3, 'scale 24/7 infrastructures', 'Managed mission-critical conglomeration', 'projektor,internet,biurko', 80.89, 1, 0x30, 0x31, 145.05, 0.74, null, 1.39),
        (133, 3, 1, '7807 Fairfield Place', 1, 'matrix synergistic convergence', 'Implemented multi-tasking standardization', 'projektor,internet,biurko', 101.36, 4, 0x30, 0x30, 606.02, 0.27, null, null),
        (134, 5, 5, '92 Morning Avenue', 1, 'embrace sticky technologies', 'Distributed 24 hour interface', 'projektor,internet,biurko', 52.27, 5, 0x31, 0x30, 936.49, 0.36, null, null),
        (135, 6, 2, '2 Lakewood Gardens Hill', 5, 'productize collaborative methodologies', 'Enterprise-wide empowering standardization', 'kieliszki do szampana,jacuzzi,hifi', 62.22, 2, 0x31, 0x31, 436.07, 0.01, null, null),
        (136, 2, 36, '00 Sommers Junction', 5, 'redefine collaborative interfaces', 'Cross-group 6th generation matrix', 'wieszaki,suszarka,kosmetyki', 2.61, 1, 0x31, 0x30, 763.19, 0.34, null, 2.2),
        (137, 12, 41, '2852 Bowman Drive', 3, 'synergize vertical platforms', 'Decentralized grid-enabled installation', 'wieszaki,suszarka,kosmetyki', 130.16, 3, 0x30, 0x31, 720.02, 0.02, null, 4.6),
        (138, 13, 34, '84 Caliangt Park', 4, 'repurpose visionary solutions', 'Inverse zero administration alliance', 'prasa, kawa, minibarek', 23.94, 5, 0x31, 0x31, 771.61, 0.05, null, 2.63),
        (139, 9, 22, '2 Spenser Junction', 1, 'aggregate extensible paradigms', 'Multi-layered systematic alliance', 'wieszaki,suszarka,kosmetyki', 63.84, 3, 0x30, 0x30, 626.84, 0.33, null, null),
        (140, 11, 44, '7291 Myrtle Hill', 5, 'evolve turn-key infomediaries', 'Synchronised radical adapter', 'pralka,mikrofalowka,rolety', 40.4, 2, 0x30, 0x31, 283.56, 0.61, null, 3.28),
        (141, 12, 18, '327 Little Fleur Crossing', 3, 'target cutting-edge channels', 'Organized regional process improvement', 'kieliszki do szampana,jacuzzi,hifi', 66.64, 5, 0x30, 0x31, 755.55, 0.63, null, 0.77),
        (142, 3, 6, '40 Farwell Alley', 2, 'recontextualize revolutionary bandwidth', 'User-centric tertiary open architecture', 'wazon, przypory kosmetyczne, swiatlowod', 38.44, 5, 0x30, 0x31, 261.49, 0.8, null, 2.1),
        (143, 8, 45, '447 West Park', 2, 'engineer web-enabled partnerships', 'Optimized needs-based circuit', 'prasa, kawa, minibarek', 121.03, 5, 0x30, 0x30, 132.92, 0.31, null, 1.3),
        (144, 3, 18, '92 Brentwood Circle', 5, 'enhance 24/365 experiences', 'Right-sized client-server leverage', 'kieliszki do szampana,jacuzzi,hifi', 14.49, 2, 0x30, 0x31, 779.42, 0.35, null, 0.5),
        (145, 3, 51, '8 Forster Plaza', 1, 'revolutionize cross-platform eyeballs', 'Persistent asynchronous parallelism', 'prasa, kawa, minibarek', 47.15, 5, 0x30, 0x31, 953.24, 0.04, null, null),
        (146, 8, 36, '9992 Sunnyside Circle', 5, 'matrix strategic e-tailers', 'Proactive stable orchestration', 'pralka,mikrofalowka,rolety', 99.31, 2, 0x30, 0x30, 574.69, 0.1, null, 2.53),
        (147, 2, 54, '02 Linden Drive', 3, 'e-enable open-source networks', 'Monitored eco-centric pricing structure', 'pralka,mikrofalowka,rolety', 90.54, 4, 0x30, 0x30, 544.34, 0.46, 'Cross-platform disintermediate capability', 3.8),
        (148, 5, 4, '6 Buena Vista Trail', 3, 'harness granular infomediaries', 'Realigned intermediate challenge', 'prasa, kawa, minibarek', 119.81, 4, 0x31, 0x30, 195.4, 0.68, 'Robust mission-critical product', 0.78),
        (149, 6, 10, '61992 Scott Alley', 1, 'empower bricks-and-clicks applications', 'Business-focused systematic toolset', 'zelazko,lozko wodne, dmuchany materac', 50.94, 1, 0x30, 0x30, 311.53, 0.06, null, null),
        (150, 1, 27, '4726 Oakridge Court', 4, 'engage B2B convergence', 'Managed systemic standardization', 'jacuzzi, waga, zastawa stolowa', 6.66, 4, 0x31, 0x30, 692.78, 0.15, null, null),
        (151, 3, 25, 'Mickiewicza 12', 12, 'Wielki aparart', 'Opis apart', 'żelazko', 34.33, 4, 0x31, 0x31, 345.43, 0, '0', null),
        (152, 3, 27, 'Mickiewicza 12', 15, 'Wielki aparart', 'Opis apart', 'żelazko', 34.33, 4, 0x31, 0x31, 100.43, 0, '0', null),
        (153, 3, 26, 'sliwinskiego 13', 3, 'SUper lokal', 'Zobacz sam', 'kawiarka, zaparzacz do ziol', 40.22, 3, 0x30, 0x31, 240.34, 0, '', null);
	

insert into user (idUser, Name, Surname, BirthDate, Address, PostCode, City, PrimaryPhoneNumber, SecondaryPhoneNumber, Email, Password, isActive, isConfirmed, isBanned, Comments, idPaymentMethod, isHost, isGuest, UserLocalization_FK)
values  (1, 'Pawel', 'Suwlacz', '1979-12-21', 'Krupnicza 12 / 18', '30-057', 'Krakow', 600122444, 532033193, 'psuwlacz@o2.pl', '882a4b67967f636bcd769a1c5e7bf7ea', 0x31, 0x31, 0x30, '', '0837959a7cd1da9ffc48363936e4a45f', 0x31, 0x31, 26),
        (2, 'Gustavo', 'Fring', '1962-03-10', 'Erasmo Escala 20 / 5', '1-39213', 'Santiago', 9433134341, null, 'honey@white.com', '2adbffe4ac3c57b5ef7d3619c5319109', 0x31, 0x31, 0x30, null, '5a9169f7efdb6e0fa980f5e0f218cd44', 0x31, 0x31, 43),
        (3, 'Josef', 'Petersille', '2000-02-12', 'Wagner Strasse 10', '13-3455', 'Berlin', 349323454, null, 'jpetersille@gmail.com', 'abca6ff4d085ffbe343b2aa93a7a62eb', 0x31, 0x30, 0x30, null, 'aa0d88a37c77cdd81bb4c6cdfee41c31', 0x30, 0x31, null),
        (4, 'Ricardo', 'Marquez', '1990-05-30', 'Pastores 2', '44-32123', 'Mexico City', 385399938, 92853892, 'rmarq@yahoo.com', '0f104b8bbd7a71bddfab685aa03bbf5c', 0x31, 0x31, 0x31, null, '455dd6cca2186589d91d9543b8e4e36a', 0x31, 0x31, 14),
        (5, 'Will', 'Smith', '1954-12-04', 'Rockefeller Street 4 / 33', '331-4343', 'New York', 335898334, 93854398, 'wsmith33@gmail.com', '2df2c0f5b920c9b85a9013a391ce57d9', 0x31, 0x31, 0x30, null, '2bc1ea8ec88d442de612351bce503168', 0x31, 0x31, 12),
        (6, 'Karol', 'Wielki', '1949-12-12', 'Hinau St', '394-43', 'Christchurch', 232493000, 93094309, 'bigchuck@gmail.com', '267a46130237b749eb95aef0206823e1', 0x31, 0x31, 0x30, null, 'e92576ab57e61ded21d087fddd41f75a', 0x31, 0x31, 23),
        (7, 'Andrzej', 'Mikrus', '2001-03-11', 'Krotka 15', '23-323', 'Radom', 504033222, null, 'malyalewariat@o2.pl', 'b01d48ae69ed4de6baf194f1effe300e', 0x31, 0x31, 0x30, null, '4eebb0ca4b4aead733c9d22493fe73a7', 0x31, 0x31, 23),
        (8, 'Konrad', 'Walenrod', '1988-04-01', 'Janka Bobetka 11/2', '39-422', 'Dubrownik', 2948298429, null, 'andriejmini@outlook.com', 'c62031c679c168fb17a3486259d1a6e5', 0x31, 0x30, 0x30, null, '8c0001796384bfff2a6ac20e20b6fcbf', 0x31, 0x31, 11),
        (9, 'Oskar', 'Rybny', '1965-12-04', 'Dragavegur 15', '34-33', 'Rejkiawik', 4984918, null, 'bigfish@gmail.com', '812d978cd21a1661b2a80c6976eb9530', 0x31, 0x31, 0x30, null, 'aace60d6be9d85f3392f9de924223207', 0x31, 0x31, 28),
        (10, 'Anna', 'Karenina', '1955-05-22', 'Dostojewskiego 4/12', '334-3094', 'Sant Petersburg', 39430222, null, 'akarenina@gru.com', '25b855fda3d1f05514904ae0a2bdf3c1', 0x31, 0x30, 0x30, null, '48c513f66321266b7f9b8d5470e0c78e', 0x31, 0x31, 53),
        (11, 'Samantha', 'Fox', '1975-02-12', 'Nishino-Tondin-Dori Ave', '343-333', 'Sapporo', 394838943, null, 'sfox@weeboo.jp', '64427413ea566c79b7cd40f24c77ad91', 0x31, 0x31, 0x30, null, '863f689ddad9611840e6cae7a4a66158', 0x31, 0x31, 1),
        (12, 'Lucas', 'Grape', '1990-11-19', 'Shalma Rd 23/1', '2-242-22', 'Tel Awiw', 29842489, null, 'lgrapeshalma@gmail.com', '25f4e6bddec6438bc446f56a0696c155', 0x31, 0x31, 0x30, null, 'a33b09530707b8afee5e8f49c1f658aa', 0x31, 0x31, 9),
        (13, 'Johny', 'Bravo', '1980-04-30', 'Rue Delfose 4', '2233-222', 'Montreal', 1983832920, 29838549, 'jbravo@gmail.com', 'a693ff1b0baae87f6c7bf907958b81e8', 0x31, 0x31, 0x30, null, '1073b3dcb7c339ba3f18640fb312a34e', 0x31, 0x31, 13),
        (14, 'Abdul', 'Alhazdred', '1981-03-01', 'Fleb Glad 3/1', '932-4345', 'Aleksandria', 4243443, null, 'aalhazdred@gmail.com', '22be6f0ae0859605838816069fe4231f', 0x31, 0x31, 0x30, null, 'e02666f2026a6967e9f1b84cb0dc191b', 0x31, 0x31, 10),
        (15, 'Julia', 'Romeo', '2002-01-18', 'Acampis 14', '4389-34', 'Cordoba', 23892482, 5534389, 'jromeo2002@gmail.com', '45be261a542cd516c2953b735674ee7a', 0x31, 0x31, 0x30, null, '5bb0da6ba2348338379c29429613e87a', 0x31, 0x31, 46),
        (16, 'Esmeralda', 'Romeriano', '1998-10-28', 'R. Liberato Barroso', '34893-34', 'Fortaleza', 3948394, 9482984, 'esmeralda.romeriano@yahoo.com', 'a332d849df262d57d3ea076997d15c7a', 0x31, 0x31, 0x30, null, 'febb1cd65993ff9c86bfaffac8e37aac', 0x31, 0x31, 51),
        (17, 'Muhsin', 'Parari', '1975-07-31', 'Avenue des Merindes 12', '123-444', 'Fez', 29839238, null, 'mparari@local.mr', '4ba824a0062e1214159968a5d67c6d29', 0x31, 0x31, 0x30, null, '8b231889b35f943dcae31244309cae44', 0x31, 0x31, 14),
        (18, 'Donald', 'Ribbon', '1968-06-04', 'Rue Dr Razafimpanilo 12/1', '23-2214', 'Antananarywa', 948934839, null, 'dribbon@gmai.com', 'ea57de325d9eafe518dfce0708aee2d1', 0x31, 0x30, 0x30, null, '4f06b15fc49367dbf0bac6e623aaefef', 0x31, 0x31, 16),
        (122, 'Hebert', 'Clowney', '1986-06-21', '935 Glacier Hill Terrace', '309534', 'Vina del Mar', 24504170, null, 'hclowney1@usda.gov', '0c2056817b5f717dd2b614dcfa512e0d', 0x31, 0x30, 0x30, '5369634173', 'cdfcbd8f2782976eb8597b82b6a52d23', 0x31, 0x31, null),
        (526, 'Arch', 'Cucuzza', '1981-02-22', '00381 Crescent Oaks Parkway', '40323', 'Kair', 40684705, 22172206, 'acucuzza3@github.com', '222f9dd75d47d872a796111cbe08a60d', 0x31, 0x30, 0x30, null, 'f8cecade63d9c9fcf1d08ac8f2905da7', 0x31, 0x31, 16),
        (527, 'Nichols', 'Shelmardine', '1984-02-03', '1316 Algoma Parkway', '4765-098', 'Las Vegas', 18621190, 73368584, 'nshelmardine4@kickstarter.com', '391e1dfa239a079516dffd7b98acd37a', 0x30, 0x30, 0x31, null, '5cea9cef63b21aaaf920be8404ecbcf7', 0x31, 0x31, 53),
        (528, 'Sansone', 'Prior', '1976-11-12', '2 Drewry Trail', '06300-000', 'Fortaleza', 57150716, 64505009, 'sprior5@163.com', '10d699abcf2f3d3b9251999c51d81538', 0x30, 0x31, 0x30, null, '91dff4336e0dae22827ad9377d5e6641', 0x31, 0x31, 24),
        (529, 'Carly', 'Cancellieri', '1986-09-06', '53 Carioca Avenue', '3707', 'Betlejem', 18181398, 35359185, 'ccancellieri6@rakuten.co.jp', 'f45dcab20f05c6e798e1acfb4b038887', 0x31, 0x30, 0x31, 'architect world-class paradigms', 'c140c13f23e1531aa191a8b715cb7694', 0x31, 0x31, 7),
        (530, 'Brnaba', 'Lecordier', '1986-08-26', '38 Johnson Point', '737 32', 'New York', 8545484, 39941151, 'blecordier9@engadget.com', 'e41c52a6d87277c0f70ca74307e8a2f1', 0x31, 0x30, 0x31, null, '5e406af1901cf0bf4754a1f74a50160d', 0x31, 0x31, 16),
        (531, 'Burnard', 'Braddon', '1969-01-22', '5648 Duke Circle', '6419', 'Cuetzalan', 40612877, 42896314, 'bbraddonb@seesaa.net', 'fc04e7ee38875cb9919e837347655576', 0x30, 0x30, 0x31, null, '693a305a42687c075c9ab201102e63df', 0x31, 0x31, 5),
        (532, 'Kermit', 'Broschke', '1969-08-06', '0667 Lunder Street', '919-0222', 'Vancouver', 71889157, null, 'kbroschked@meetup.com', '3321c1e1e560725f2f395adf26ac9d1d', 0x30, 0x31, 0x31, null, '95d2e980a4e43c33f5e48bc5548b0796', 0x31, 0x31, 26),
        (533, 'Kendall', 'Loines', '1979-02-05', '08498 Fallview Hill', '55151', 'Vina del Mar', 87648483, 77499888, 'kloinese@bing.com', 'b38d0dabc3421a5cae4cd2e2872f4bf0', 0x30, 0x30, 0x30, null, '4e0ece226ded4d1c225043878596c669', 0x31, 0x31, 44),
        (534, 'Gunner', 'Dunbobbin', '1998-08-27', '10122 Farmco Terrace', '735538', 'New York', 69784917, null, 'gdunbobbinh@cbc.ca', '883fa5f405103814102e8022b464eaf0', 0x30, 0x31, 0x30, null, 'a443540eba82c5b62d8b13264e5c01cb', 0x31, 0x31, 13),
        (535, 'Trumann', 'Desbrow', '1978-05-28', '50 Ludington Road', '45150', 'Zadar', 90822971, null, 'tdesbrowi@dailymotion.com', '048ff29ebec0064c09cf5dddd15d667b', 0x31, 0x30, 0x31, 'e-enable leading-edge ROI', '93d2d0eac1112bbb648086e57ae3194c', 0x31, 0x31, 45),
        (536, 'Gay', 'Westmacott', '1980-11-24', '97 Kedzie Court', '163028', 'Kraków', 74171442, null, 'gwestmacotto@bbc.co.uk', 'a07631f521cbf9f78e76a4d44e764afa', 0x31, 0x31, 0x31, null, '449549725281be3c28398420767b7fac', 0x31, 0x31, 45),
        (537, 'Michael', 'Gentric', '1959-01-10', '41169 Victoria Way', '533 52', 'Vina del Mar', 8842521, 66944607, 'mgentricr@biblegateway.com', 'f7e425625fbdbdee31b87fd5a4b844de', 0x31, 0x30, 0x30, null, 'beaf1d9204ba9dab2cad12dc9c8f2a62', 0x31, 0x31, 53),
        (538, 'Doyle', 'Hallwood', '1967-10-02', '637 Morningstar Court', '345-0036', 'Melbourne', 58505937, null, 'dhallwoods@dot.gov', 'b8c31e3d6d34db807283f58fc2bfd940', 0x31, 0x30, 0x30, null, '8f96bc5126927e0fb9eccca4a585fba5', 0x31, 0x31, 7),
        (539, 'Trueman', 'Carneck', '1965-02-23', '50 Katie Road', '4105', 'Kair', 33624229, null, 'tcarneckt@bluehost.com', '4954d496d51e03cffad83cd7b9607b3d', 0x31, 0x30, 0x31, null, '43a2eadc97f7d41ffae01dc50e939b59', 0x31, 0x31, 18),
        (540, 'Cori', 'Yablsley', '2000-08-16', '12356 Schiller Circle', '6501', 'Miasto Meksyk', 54820592, null, 'cyablsleyu@shutterfly.com', 'bbf1de6523945bd37c5aef9aec459f31', 0x30, 0x30, 0x30, null, 'bc222e1c315ef6ab8fe27eb91484105b', 0x31, 0x31, 44),
        (541, 'Hugh', 'Canavan', '1988-03-29', '4496 Mayer Pass', '30220', 'Rejkiawik', 8667694, null, 'hcanavanv@mapy.cz', '29c045ead5244ef1a15d4426b73b9945', 0x30, 0x31, 0x31, null, '0a31bd94060df958c7af7a35a3f10ccd', 0x31, 0x31, 37),
        (542, 'Killie', 'Joly', '1988-05-30', '32409 Elmside Way', '191017', 'General Roca', 84072525, null, 'kjolyw@feedburner.com', 'e292208c913ce29c88f87fc1dbd34f6d', 0x31, 0x31, 0x31, null, 'a5def4255f2ee3cc4d6e519e38a59d2f', 0x31, 0x31, 46),
        (543, 'Jeramey', 'Nimmo', '1965-08-30', '6843 Declaration Street', '22730', 'Melbourne', 96469056, null, 'jnimmoy@deliciousdays.com', 'ae6154b2d39c77034cb574e4f53e580b', 0x30, 0x31, 0x30, null, '1a31a827d3e303791196476e4717a614', 0x31, 0x31, 25),
        (544, 'Pooh', 'Prynne', '1967-06-12', '37 Pearson Junction', '30316', 'Tel Awiw', 47288959, 33025774, 'pprynnez@chron.com', 'd5f069b71eddc367cefc9d760ac95540', 0x30, 0x31, 0x30, 'maximize scalable e-markets', '37e085beaac648984f4d3b73b6f999e1', 0x31, 0x31, 17),
        (545, 'Roderick', 'Maberley', '1950-09-10', '48 Shoshone Alley', '439 82', 'Jerozolima', 26507812, 68538242, 'rmaberley11@seesaa.net', 'aaad0b323c9acf73174ad2a83cd92828', 0x31, 0x30, 0x31, null, '27776c5a4ddd831192989a082681a22d', 0x31, 0x31, 16),
        (546, 'Maximilianus', 'Seely', '1968-10-05', '5590 Sutteridge Park', '5210-105', 'Sydney', 24884101, null, 'mseely12@netvibes.com', '0d9dd12a43869619d9688363f47440ec', 0x31, 0x30, 0x30, null, '7313643bcb5d591741448a8e6847a2fc', 0x31, 0x31, 44),
        (547, 'Carey', 'Triswell', '1994-04-03', '06872 Bartelt Terrace', '34934 CEDEX 9', 'Antananarywa', 35449212, 48504713, 'ctriswell14@w3.org', '6546911ca9131f9d830bf316a4c3cc89', 0x31, 0x31, 0x31, null, 'e445a0e7a54feb6a1090742a8d3daae9', 0x31, 0x31, 46),
        (548, 'Harwell', 'Hallsworth', '1967-06-12', '24539 Meadow Valley Center', '253449', 'Cuetzalan', 55235985, 91008683, 'hhallsworth1b@weebly.com', 'b4e3eaf692e130d26baff2a07b3c94d7', 0x30, 0x30, 0x31, null, 'f5db298e1bf1a1aa4033abcf556085fb', 0x31, 0x31, 33),
        (549, 'Guthrie', 'Walster', '1952-05-07', '94949 Welch Circle', '13270', 'Luksor', 86351534, 63861768, 'gwalster1d@icio.us', '0359c4d415666d071cac7c19995175e6', 0x31, 0x30, 0x31, null, '3691948dbe8508fcbd63f3adaabbfd22', 0x31, 0x31, 35),
        (550, 'Lauritz', 'Gateshill', '1992-07-12', '6 Garrison Alley', '79042 CEDEX 9', 'Cordoba', 67709022, 60514909, 'lgateshill1g@berkeley.edu', '7dd1947d383acfa854694dcc4aaee85f', 0x30, 0x30, 0x31, null, '07e07cdcd90aed943b40c3f2a16e44f7', 0x31, 0x31, 5),
        (551, 'Garner', 'Brunker', '1952-08-26', '92609 Sage Terrace', '42-152', 'Christchurch', 91677791, null, 'gbrunker1i@state.tx.us', '71e177dc0923ce528b35935cc8cc3485', 0x30, 0x30, 0x30, null, '8d746ba6c123445b4db62f4208d33a0d', 0x31, 0x31, 32),
        (552, 'Teddie', 'Somerled', '1993-07-18', '960 Katie Junction', '448 92', 'Sydney', 43767180, 2107614, 'tsomerled1j@businesswire.com', 'd6c7af1fd0686db263fd32ec0b488b4e', 0x31, 0x30, 0x30, null, '5e3601b03427e1fd75d0c84358653d62', 0x31, 0x31, 54),
        (553, 'Clim', 'Mundell', '1974-01-30', '2573 Dixon Pass', '100 73', 'Kair', 29908756, 24659192, 'cmundell1k@tinypic.com', '5c2121462d96027c160f1e8dcb775dbb', 0x30, 0x30, 0x31, 'empower B2C niches', 'c361b703eb76940132fc44b883fe8ded', 0x31, 0x31, 52),
        (554, 'Ellswerth', 'Dugan', '1999-08-21', '22546 Linden Lane', '142641', 'Hokkaido', 85941789, null, 'edugan1o@macromedia.com', '2118be9bbccea36e78b2fa45fca741ca', 0x30, 0x30, 0x31, null, 'fb63ae1521613f95ddeb9ffe969a7958', 0x31, 0x31, 46),
        (555, 'Hernando', 'Mogey', '1979-11-17', '6789 Lawn Park', '1200', 'Jeju', 66067949, null, 'hmogey1u@issuu.com', '85065182c77fa9854d00345eac9e7388', 0x30, 0x30, 0x31, null, 'e773da9af077476bd0dfbd9e8eafd6fb', 0x31, 0x31, 28),
        (556, 'Parry', 'Besnard', '1950-10-25', '31 Green Park', '7416', 'Luksor', 10434479, 26242574, 'pbesnard1v@twitter.com', 'b4a5489a6db0dde58d3d0463b4dc081d', 0x30, 0x30, 0x30, null, 'b98380453120d5f562715bce950ed04e', 0x31, 0x31, 39),
        (557, 'Bary', 'Addicote', '1950-03-13', '94017 Spenser Circle', '89665-000', 'Jeju', 90175133, null, 'baddicote1y@themeforest.net', '644515756533bb2a893972725b1abb4f', 0x30, 0x31, 0x30, null, '34d216df711074952744117a2eadc5bc', 0x31, 0x31, 42),
        (558, 'Stanislas', 'Forsard', '1972-04-05', '183 Meadow Valley Place', '92892 CEDEX 9', 'Kraków', 77120012, null, 'sforsard1z@twitpic.com', '69d9ef070bb04fdaa2935aacfdf87b57', 0x31, 0x31, 0x30, 'integrate killer channels', '6c7a25e199be6de0ffbfe6180de1f897', 0x31, 0x31, 22),
        (559, 'Cheston', 'Toyne', '1978-05-25', '60 7th Pass', '21-404', 'Santiago', 76728528, 94648985, 'ctoyne23@cnbc.com', '827ade877ff0bdea1b80727e73e37969', 0x31, 0x31, 0x31, 'seize proactive schemas', 'b33510747f57914a59afc54e796c8616', 0x31, 0x31, 12),
        (560, 'Si', 'Fortune', '1998-01-24', '3 Debra Alley', '97701 CEDEX 9', 'Warszawa', 46348715, 97697563, 'sfortune25@techcrunch.com', 'f82a47742d7af337ef7896ec39e8265e', 0x31, 0x30, 0x31, null, 'd345ce4db4697e316d94b751f632e24b', 0x31, 0x31, 38),
        (561, 'Brose', 'Japp', '1987-05-24', '54159 Memorial Street', '21230', 'Marrakesz', 85056282, null, 'bjapp28@smh.com.au', 'edae58fb4a510599fe17246ff0d1588d', 0x30, 0x30, 0x31, null, '50ca0757f88df727abf372a15afff543', 0x31, 0x31, 14),
        (562, 'Urbano', 'Soar', '1950-06-21', '7789 Steensland Lane', '97001', 'Casablanca', 5224127, 22377205, 'usoar29@miitbeian.gov.cn', '202a3d0f766ca97e1f44addabc1415c3', 0x30, 0x30, 0x31, null, 'ad563e5c8f5207abc2ab0f0fb03afa59', 0x31, 0x31, 29),
        (563, 'Chester', 'Cooley', '1982-01-25', '5702 Prairie Rose Road', '356 04', 'Sapporo', 86556042, null, 'ccooley2a@ucsd.edu', 'f57b3093ec8906f6afdae4dfde9b0fde', 0x31, 0x30, 0x30, null, '74b88a5a7cb91aafc4adbc964fc208d3', 0x31, 0x31, 48),
        (564, 'Torr', 'Valadez', '1981-05-12', '22 Warner Terrace', '416102', 'Perth', 53126391, null, 'tvaladez2c@reuters.com', '2f84692d0044da1d2ec1269c6659ffc6', 0x31, 0x31, 0x31, null, '9a58e1008af72b51c2ba7c3d98437e67', 0x31, 0x31, 53),
        (565, 'Nikos', 'Flint', '1980-06-01', '1439 Talmadge Place', '20355', 'Zagrzeb', 85312162, null, 'nflint2d@tripadvisor.com', '8b6fd10dfee0a46e0fa3dd1ba63f2161', 0x30, 0x31, 0x31, null, '46dc703f2380258b6ea0fc65344d2ac5', 0x31, 0x31, 25),
        (566, 'Hervey', 'Barwick', '1968-02-23', '91 Emmet Plaza', '147 00', 'Jeju', 77867794, 91020123, 'hbarwick2i@paypal.com', '9c3a66a2b9ddeb90b2ed089b172fb6c3', 0x31, 0x31, 0x30, null, '8ba242f55e5956e1afe0163bf7a3c48d', 0x31, 0x31, 47),
        (567, 'Karl', 'Roskams', '1971-09-28', '32 Vahlen Place', '457 81', 'Fez', 25235303, null, 'kroskams2j@rediff.com', '0ce2d09218cf5d5e7b4451fe71717ec0', 0x31, 0x31, 0x30, null, 'adeaa39993849e0c6c1fd8d2588fd7d5', 0x31, 0x31, 50),
        (568, 'Ramsey', 'Cove', '1984-05-01', '157 Schlimgen Street', '38504 CEDEX', 'Las Vegas', 65820324, null, 'rcove2k@hostgator.com', '4e9d7cfd095c18cb7ee2421630a7bec6', 0x31, 0x31, 0x31, 'incubate open-source communities', '7f2242c98acc1a14441cc794d9bfccf1', 0x31, 0x31, 4),
        (569, 'Yorke', 'Heighway', '2000-10-31', '5130 Helena Court', '690922', 'Kraków', 79374707, null, 'yheighway2l@comcast.net', '460c0196318e0df32b1aaa070bfe34ba', 0x31, 0x31, 0x30, null, '72cf5998dddc5998060f962620f764f6', 0x31, 0x31, 10),
        (570, 'Welsh', 'Lamperd', '1974-06-16', '6013 Park Meadow Lane', '84304 CEDEX', 'Miasto Meksyk', 87909055, 24661249, 'wlamperd2o@unblog.fr', 'f8e4ccabb5a82d192a83b969cf54f065', 0x30, 0x30, 0x31, 'syndicate virtual e-commerce', 'efb42444d24ad71809405932112da742', 0x31, 0x31, 33),
        (571, 'Emanuel', 'Passmore', '1972-02-28', '5132 Esker Junction', '8424', 'Miasto Meksyk', 28224716, null, 'epassmore2q@4shared.com', 'd235f971e8550774a608ae9f7a349647', 0x30, 0x30, 0x31, null, '29ccc2af0f9b8b14c0d7981f3aad8b13', 0x31, 0x31, 47),
        (572, 'Ezri', 'Shillabear', '1991-11-29', '3415 Shoshone Drive', '353210', 'Novotitarovskaya', 16527776, null, 'eshillabear1@godaddy.com', '89501243083b53fd3389ae24ff90ca10', 0x30, 0x31, 0x31, null, '9e5f68c17b1605ed98d49e92c32c0621', 0x30, 0x31, null),
        (573, 'Michel', 'Cantor', '1981-08-07', '901 East Street', '75195 CEDEX 04', 'Paris 04', 39030692, null, 'mcantor2@archive.org', 'e2efe27e591eb8970b37ab39c0bcf50f', 0x31, 0x31, 0x30, null, 'a32bcbff0c86943547005c536f92b21b', 0x30, 0x31, null),
        (574, 'Rockwell', 'Rookesby', '1980-08-07', '5 Old Gate Junction', '40000', 'Jaffna', 10600553, null, 'rrookesby3@so-net.ne.jp', '2f848a3a1829d05f73ad7504f972bf0d', 0x30, 0x31, 0x31, null, '0211dd5405f2fc96d0533e3e99e8ac07', 0x30, 0x31, null),
        (575, 'Fran', 'Leap', '1963-03-01', '4 Manitowish Pass', '50943', 'Bunga Mas', 23965426, null, 'fleap4@wisc.edu', 'e1e7d11cddeb22233a253d5ce82a45cb', 0x31, 0x30, 0x30, null, '5ed14082743b8040db5769c168896ca5', 0x30, 0x31, null),
        (576, 'Vaughn', 'Physic', '1991-06-28', '01188 Hazelcrest Court', '403932', 'Mumen', 69485123, 58529830, 'vphysic5@sun.com', '01876c91bbc6104d50af181223e251d6', 0x30, 0x30, 0x30, 'monetize customized niches', '68b9dbd8bdac188184ec7aabfd4d49d6', 0x30, 0x31, null),
        (577, 'Mozes', 'Venart', '1973-04-01', '0 Lakeland Place', '534JJJ', 'Baimajing', 43573512, null, 'mvenart6@yolasite.com', '5d677f692745f3ec84e40e6b6b345e94', 0x30, 0x31, 0x30, null, 'c48e8e467af22c1941591d0762681917', 0x30, 0x31, null),
        (578, 'Jessee', 'Brunone', '1962-02-07', '4 American Ash Parkway', '8720', 'Del Pilar', 36492945, 29308438, 'jbrunone7@wired.com', '3b542de5633ad6bcbfe2db4d3465c8d8', 0x31, 0x30, 0x31, null, 'f0c8b47ef52785b1b5afad7579e37c8d', 0x30, 0x31, null),
        (579, 'Padget', 'Ponnsett', '1973-04-30', '46 Gateway Way', 'SF-343', 'Yankou', 60320059, null, 'pponnsett8@nbcnews.com', '1254d6579046f1ac1ff5043f036dbbe8', 0x31, 0x31, 0x31, null, '986e2eea6a7024db72143a3c24e67660', 0x30, 0x31, null),
        (580, 'Dana', 'Albury', '1991-08-01', '3 Ridge Oak Plaza', 'OR-43434', 'Qal‘ah-ye Na‘īm', 66626000, null, 'dalbury9@purevolume.com', '397a7451996552ddbc7fc0f45c02240a', 0x30, 0x31, 0x30, null, '5814f48a8c27298be5c8284892748f48', 0x30, 0x31, null),
        (581, 'Monroe', 'Cunnane', '1991-07-22', '5251 Reindahl Terrace', 'L-7346', 'Steinsel', 5537562, 6089162, 'mcunnanea@ed.gov', '81b13d2fa972815fdfef16c5fe7dcb95', 0x30, 0x31, 0x30, null, 'c7f1aa21f727f793a35002f3b0923c2f', 0x30, 0x31, null),
        (582, 'Germaine', 'Viles', '1970-10-18', '42 Bobwhite Parkway', '9835', 'Ivyanyets', 69172384, 65821736, 'gvilesb@networksolutions.com', '85d910911228c5290d9f0731bff7cd26', 0x30, 0x30, 0x30, null, '14bd905ac4fc9f876e4dc82240aa8243', 0x30, 0x31, null),
        (583, 'Sol', 'Jannings', '1957-12-01', '0891 Marquette Hill', '5005', 'Gibato', 94255717, null, 'sjanningsc@yahoo.com', 'a201bb3e48f0121c3223fbca167a8bc6', 0x30, 0x31, 0x31, 'harness granular content', '7be383af6100bbbf83d8672d4ec222ed', 0x30, 0x31, null),
        (584, 'Jephthah', 'McClean', '1954-01-23', '5 Anhalt Plaza', '9545-524', 'São Vicente de Ferreira', 47536745, 2818995, 'jmccleand@redcross.org', '2077eebde392ce7c412e81fe77aed081', 0x30, 0x30, 0x31, null, 'b469a363c84a5db419eba900910480dc', 0x30, 0x31, null),
        (585, 'Heindrick', 'Biggin', '1965-02-25', '197 East Street', '353223', 'Mancos', 20203991, null, 'hbiggine@google.de', 'bf65a7f01695cb0ed1c6ef0a4967a71a', 0x30, 0x30, 0x30, null, 'd3a6cb4d08189d58d3bee30041c35a39', 0x30, 0x31, null),
        (586, 'Henrik', 'Shevelin', '1965-03-05', '85 Bonner Court', '8113', 'Sampao', 23115258, null, 'hshevelinf@yahoo.co.jp', '33bf62ef15fc2e88d203630c3451a935', 0x30, 0x30, 0x31, null, 'c1bd4f2acc9afc09357a6eebc90e2c2d', 0x30, 0x31, null),
        (587, 'Rudolph', 'Higgen', '1974-05-27', '1 Sutherland Center', '2424', 'Oropéndolas', 59210627, null, 'rhiggeng@youtube.com', 'f60612983b94d422c6700f35625d945d', 0x30, 0x30, 0x31, null, '7010474e64b6402747f92a641cf645ce', 0x30, 0x31, null),
        (588, 'Oliviero', 'Hursthouse', '1980-10-30', '596 Bunting Way', '42242', 'Banjar Buahan', 85005548, 39032181, 'ohursthouseh@ucoz.com', '4296e9c6f153119812f7c62d865f45de', 0x31, 0x31, 0x31, null, '83925f9596e20e18988b05829b34e51b', 0x30, 0x31, null),
        (589, 'Gregorius', 'Stainer', '1994-05-01', '7754 Melrose Trail', '64701', 'Teuva', 62281565, null, 'gstaineri@oakley.com', '7fac7588413d37d4e55de16024df7d1f', 0x31, 0x30, 0x30, null, '358c49e55f1f21c3050c34efb4d2cb93', 0x30, 0x31, null),
        (590, 'Clarence', 'Hawke', '1973-12-16', '860 Hauk Point', '633343', 'Bolotnoye', 6859072, null, 'chawkej@ft.com', 'ae9fead60cfd3b07031431cefc421c63', 0x31, 0x31, 0x30, null, '4738c81afb0302e5b37c7eff00f6c62f', 0x30, 0x31, null),
        (591, 'Matthaeus', 'Cristofolo', '1991-06-21', '92 Manley Center', '66-436', 'Słońsk', 48266769, 48290559, 'mcristofolok@gmpg.org', '828bde761e38aa848b6d1a776d7c688b', 0x30, 0x31, 0x30, null, 'c0d4321236ee1df10e3907d242f3a979', 0x30, 0x31, null),
        (592, 'Chet', 'Fritschmann', '1979-10-26', '6705 Quincy Plaza', '612091', 'Nizhneivkino', 18681960, 16387224, 'cfritschmannl@yahoo.com', 'ffbc833bf5bf11201dd99655715d9c96', 0x31, 0x30, 0x30, null, '357997efdd766cdd08966df0f42b6895', 0x30, 0x31, null),
        (593, 'Son', 'Galloway', '1964-01-12', '85991 Westend Terrace', '424-242', 'Checun', 13654353, 35051969, 'sgallowaym@usatoday.com', '3635094995fd818c49983211d19eb9cb', 0x30, 0x31, 0x30, null, '207c26a6555516937542db4913a78d4d', 0x30, 0x31, null),
        (594, 'Robinet', 'Imloch', '2000-11-12', '386 Aberg Crossing', 'JK-3432', 'Mishixiang', 9574413, 94492230, 'rimlochn@chicagotribune.com', '9997d666fea6f3dc3591cc74e105db97', 0x31, 0x31, 0x31, null, 'e2a9518d4515cbaa3a467ff0b22d3ab6', 0x30, 0x31, null),
        (595, 'Korey', 'Dyer', '2000-04-02', '969 Fuller Avenue', '35588-000', 'Arcos', 19852202, null, 'kdyero@youku.com', '815538d3aefb96aecd0f58405e530b51', 0x31, 0x30, 0x30, 'innovate distributed interfaces', 'ddaf3ee21059718b62722b0fd5cc3715', 0x30, 0x31, null),
        (596, 'Tommy', 'Aucoate', '1998-07-06', '9 Tennessee Pass', '8405', 'Placer', 95006119, null, 'taucoatep@moonfruit.com', '386a9a5cf48929846587bb3c3e708b09', 0x30, 0x30, 0x30, null, 'bcce3b16936c18e5b0f69dd3eb2f45d3', 0x30, 0x31, null),
        (597, 'Dewitt', 'Merfin', '1986-10-19', '5 Summerview Drive', '353721', 'Staroderevyankovskaya', 93916287, 88248815, 'dmerfinq@unblog.fr', 'fb916a0b1f10d00c5e3de188a7628a2e', 0x31, 0x30, 0x30, null, '690efe1979dc418437ac9124fb3b13c1', 0x30, 0x31, null),
        (598, 'Rossie', 'Spikins', '1966-02-11', '0 Del Sol Court', '348-4343', 'Ambelón', 12295821, 14729827, 'rspikinsr@hatena.ne.jp', 'a159f97a1b3da6105b8bb4b81eda6117', 0x31, 0x30, 0x31, null, 'ac155ac57b67abcc9630bf170bdf0d46', 0x30, 0x31, null),
        (599, 'Tris', 'Gradly', '1984-02-18', '7866 Kinsman Drive', 'HFP853', 'Chardonnière', 46373310, 84839446, 'tgradlys@pinterest.com', '837ef996b05b69e8cf45bc48a1acc6e1', 0x30, 0x31, 0x30, 'syndicate ubiquitous solutions', 'ac6521e90ef736da309c4ca3c2d9b84f', 0x30, 0x31, null),
        (600, 'Tobin', 'Challenor', '1951-10-04', '7073 Packers Pass', '751 87', 'Uppsala', 83859887, null, 'tchallenort@jigsy.com', '2fc7d6bec75dc60420570d6afa0f1eec', 0x31, 0x30, 0x31, 'synergize sexy e-tailers', '74e91bdcd6ea3370cd6a0d3bd39b3b60', 0x30, 0x31, null),
        (601, 'Bo', 'Flanne', '1959-02-08', '8 West Way', '9834', 'Tirana', 92462027, 7015535, 'bflanneu@who.int', '8556501e985885093a35c574b4802d33', 0x31, 0x31, 0x30, null, '3ea8e99c3ad841b9562d7f7fb2baa30e', 0x30, 0x31, null),
        (602, 'Orton', 'Greenshields', '1953-12-14', '94 Rockefeller Trail', '10007', 'San Lorenzo', 84572135, 90161011, 'ogreenshieldsv@si.edu', 'd6744edc31c21a4978f49173ab9bfd33', 0x31, 0x30, 0x30, null, '26f4b25520f2958e77ae362c38549700', 0x30, 0x31, null),
        (603, 'Emmerich', 'Astman', '1964-06-10', '5368 Swallow Point', '663 30', 'Skoghall', 91400168, 27290483, 'eastmanw@vkontakte.ru', 'fadefec86f8c9a4bc422e42198ddb549', 0x31, 0x30, 0x31, 'integrate cross-platform models', '877a630d546ebb8ff638d344938707a8', 0x30, 0x31, null),
        (604, 'Gherardo', 'Dunnion', '1973-03-24', '5 Susan Court', '10270', 'New York City', 15793706, null, 'gdunnionx@hibu.com', '2ca1395c3ebead428de1f2ea6317a3f9', 0x30, 0x30, 0x31, null, '2cb13c8ef503dba2aa0391aea4549ac5', 0x30, 0x31, null),
        (605, 'Lemmy', 'Cassin', '1974-07-15', '3 Grayhawk Lane', '3702', 'Bambang', 48910486, null, 'lcassiny@github.com', '3cefa0b119fdb8e939e30544bb132cd5', 0x31, 0x31, 0x30, null, 'eadc0c02581f096cb4e3fb27e7d1c31e', 0x30, 0x31, null),
        (606, 'Morse', 'Winton', '1993-08-26', '62 Vernon Drive', '39484', 'Panjāb', 84260118, 21114336, 'mwintonz@about.me', '8fb39ebe23a846af0490e0974039a8b9', 0x31, 0x31, 0x31, null, '93160367b645eda32effc2c1bbe784cf', 0x30, 0x31, null),
        (607, 'Rudolf', 'Ales', '1976-10-12', '6276 Farmco Road', '4930-250', 'Insua', 55028422, 46567434, 'rales10@infoseek.co.jp', 'fd2e596cea4bf0c9c7f12eb66cd720a4', 0x30, 0x30, 0x31, null, '399b81f0c5aca0641b3e1ae499367492', 0x30, 0x31, null),
        (608, 'Fons', 'Lissimore', '1993-09-19', '29160 Moland Drive', '4005', 'Stavanger', 54386313, null, 'flissimore11@gizmodo.com', '5de1be2de5391c2ec8c10d9511a810e9', 0x31, 0x30, 0x30, null, 'fb9388e10e3f4bd5f8106e42f3a67a28', 0x30, 0x31, null),
        (609, 'Darrell', 'Christer', '1955-05-18', '2289 Rutledge Circle', '34-143', 'Lanckorona', 52219576, null, 'dchrister12@illinois.edu', '1884c37ae1c311cf169ad5f2b1b72d95', 0x31, 0x30, 0x30, null, '86da4b216456bc69d2a1b00594bd4204', 0x30, 0x31, null),
        (610, 'Shaughn', 'Keller', '1965-01-14', '749 Merry Alley', '434334', 'Maundai', 89975452, 92163248, 'skeller13@pagesperso-orange.fr', '4445cc99a24c6f15149b94a01dd9be7b', 0x30, 0x30, 0x31, null, '2bbd7cf4bea09ee1fefd60f60a1bdf13', 0x30, 0x31, null),
        (611, 'Duke', 'Kruszelnicki', '1952-09-09', '29555 Farragut Circle', '232', 'Tiebiancheng', 94541885, null, 'dkruszelnicki14@amazon.co.jp', '1ed60db5828d197ce388b5ad7597c4ff', 0x30, 0x30, 0x31, null, 'f31c79edbdfad7581d2ca8931db78a0c', 0x30, 0x31, null),
        (612, 'Godfrey', 'Davidou', '1966-04-19', '3762 Laurel Pass', '371', 'Oslo', 29643828, 21106784, 'gdavidou15@live.com', 'f0c059b5343560e387e9402ae5cb3114', 0x31, 0x31, 0x31, null, '75fbdf246ba259f0070392df1c7adb2c', 0x30, 0x31, null),
        (613, 'Davis', 'Benjamin', '1978-03-14', '5 Alpine Parkway', '4242', 'Kuangsan', 37656432, null, 'dbenjamin16@liveinternet.ru', '1fedc833c960a6a50a83b08f7ede2489', 0x30, 0x30, 0x30, null, '552ef5b2b3d9fb2993759253f6d3ea93', 0x30, 0x31, null),
        (614, 'Hadleigh', 'Croasdale', '1994-05-05', '8880 Doe Crossing Crossing', '444038', 'San Juan del Cesar', 9722818, null, 'hcroasdale17@tripod.com', 'dddb0be97bf91225938a1c71fde87516', 0x30, 0x30, 0x31, null, '046feb76fae6e8d626e9dd01aad86d5b', 0x30, 0x31, null),
        (615, 'Lindsay', 'Le Fleming', '1976-09-11', '4167 Portage Court', '681 54', 'Kristinehamn', 79002352, null, 'llefleming18@intel.com', '7e0104d17a8c4528e639e21d852d1006', 0x30, 0x31, 0x31, null, 'ab34cf7cb5782a2f73b15a7ced4dc04a', 0x30, 0x31, null),
        (616, 'Seymour', 'Venner', '1956-07-16', '702 Dahle Hill', '3432', 'Gastoúni', 74077432, null, 'svenner19@microsoft.com', '2287a1a47c9fd18f4cacc073034dfe70', 0x31, 0x31, 0x31, null, '9dd1ff54f45152f0a68d18a886e81bbc', 0x30, 0x31, null),
        (617, 'Neale', 'Lenormand', '1998-09-05', '2 Nelson Point', '3131', 'Banjar Sembunggede', 23903472, null, 'nlenormand1a@php.net', '91dd0d2c023e5dba21a5ef05ce9c1966', 0x30, 0x31, 0x30, 'mesh front-end schemas', '300fefb2918e22e0f4f372d43b2d0967', 0x30, 0x31, null),
        (618, 'Mar', 'Brenston', '1983-05-10', '853 Muir Way', '13617 CEDEX 1', 'Aix-en-Provence', 85731370, null, 'mbrenston1b@salon.com', '1e2575c13b706bc722ba83e2d795e220', 0x31, 0x31, 0x30, null, 'fac5339f156bac1de014e0e83d905d51', 0x30, 0x31, null),
        (619, 'Port', 'Giacovazzo', '1958-06-04', '14 Barby Point', 'YR9384', 'Labrang', 23001873, null, 'pgiacovazzo1c@paypal.com', '192411ed8e0925a9c1e3eebeb60d5852', 0x30, 0x30, 0x31, null, '2271699686244b733ce36af8e70526b5', 0x30, 0x31, null),
        (620, 'Myrvyn', 'Dagg', '1997-01-30', '598 Sauthoff Road', '33416', 'West Palm Beach', 84257618, null, 'mdagg1d@phoca.cz', 'fa51cb7a9cff1555752686a4363c5878', 0x30, 0x30, 0x31, null, '8ff7d48a10c131bd6fed361c4b4090f0', 0x30, 0x31, null),
        (621, 'Lincoln', 'Smissen', '1988-11-03', '57464 Barnett Point', '242', 'Dagur', 39050715, null, 'lsmissen1e@patch.com', '7f1e2f9cb35826ac316f9c176391a0c9', 0x30, 0x30, 0x30, 'incubate strategic e-markets', 'a2b6ebc21860a73363fde2d4795a8459', 0x30, 0x31, null),
        (622, 'Will', 'McGinney', '1998-10-27', '1450 Mccormick Trail', '1231', 'Beopwon', 95661918, null, 'wmcginney1f@walmart.com', 'd1d111c7d42789cb6f3b742fda0e4ed0', 0x31, 0x31, 0x30, null, '76842ab40f0f351e8fffce45299a11d9', 0x30, 0x31, null),
        (623, 'Hector', 'Penk', '1974-09-29', '4520 Fairview Way', '141662', 'Vostryakovo', 88880968, null, 'hpenk1g@sbwire.com', 'bac070ba21761de427715076a11b686e', 0x30, 0x31, 0x31, null, 'b8f2652fcb98238c616899c91ee13a7b', 0x30, 0x31, null),
        (624, 'Graehme', 'Bodill', '1963-08-29', '8 Bowman Terrace', '31500', 'Martin', 56682197, null, 'gbodill1h@slashdot.org', 'aae4cdcf7b6618109a7cbc549975a1ea', 0x30, 0x31, 0x31, null, 'b0b12edfa17e5c40b6c742de364f00dc', 0x30, 0x31, null),
        (625, 'Alejoa', 'Doxsey', '1972-05-29', '6308 Buell Drive', '2383', 'Legen', 9056308, 66872419, 'adoxsey1i@sourceforge.net', '141df7df634a2eaa3664e89ca14e5c2c', 0x31, 0x31, 0x31, null, 'a02995b571c2c54a6f7950e6496587e0', 0x30, 0x31, null),
        (626, 'Austin', 'Petricek', '1993-02-18', '13298 Sachs Parkway', '393925', 'Zheleznodorozhnyy', 86564231, null, 'apetricek1j@cbsnews.com', '30fdcb3d8f9b2d78acdb6dc9462db0bb', 0x30, 0x30, 0x31, null, '73eb0c4af5cab50a1c80a0aba35a5734', 0x30, 0x31, null),
        (627, 'Emanuele', 'Petett', '1966-06-19', '8 Sutteridge Crossing', '658760', 'Pankrushikha', 99553873, null, 'epetett1k@cdbaby.com', '1e1f628f9dc118f5a5faa947f79fa644', 0x31, 0x31, 0x31, null, '26d37ce7bc8f80c420ebfee42486b7af', 0x30, 0x31, null),
        (628, 'Shannon', 'Ambroisin', '1994-03-23', '952 Norway Maple Park', '18560-000', 'Iperó', 13314328, 25494803, 'sambroisin1l@delicious.com', '836f9263521849d7759d1bce2b0ad16a', 0x30, 0x31, 0x30, null, 'f6d2c3af897e9bfcc7b2c1e8b4f2e4ed', 0x30, 0x31, null),
        (629, 'Quinn', 'Lavington', '1959-02-21', '48306 Carioca Park', '164020', 'Zarechnyy', 80474865, null, 'qlavington1m@i2i.jp', 'e28eb693e0e313a939a00a794a587031', 0x31, 0x30, 0x31, null, '13abb5a26e6cb7e60296972774ca63a6', 0x30, 0x31, null),
        (630, 'Humberto', 'Schimon', '1989-03-14', '8446 Basil Park', '15500-000', 'Votuporanga', 83829992, null, 'hschimon1n@baidu.com', '03f83f36c4cf46b1f2cf3f8b2873c2eb', 0x30, 0x30, 0x31, null, '07b8f126c224b905e2bc18dde4a501c7', 0x30, 0x31, null),
        (631, 'Giacobo', 'Alyukin', '1952-01-20', '29493 Veith Circle', 'UIR9343', 'Antalaha', 24224520, null, 'galyukin1o@csmonitor.com', 'a780d04d8229ee579ca13089cb469aa8', 0x31, 0x30, 0x31, null, 'c621c80cafce9f4efc0f973bc50cf293', 0x30, 0x31, null),
        (632, 'Tomas', 'Bolle', '1956-01-31', '8253 Pearson Crossing', '23800-000', 'Itaguaí', 30598587, 42446007, 'tbolle1p@homestead.com', '9478826517679680162c29eb3d3d375e', 0x30, 0x31, 0x30, null, '4d2315b5b55545dbbb4b4d41d3e5e84d', 0x30, 0x31, null),
        (633, 'Abbe', 'Sollitt', '1981-01-06', '7 Crest Line Place', '242', 'Az Zarbah', 15610326, 9484521, 'asollitt1q@wikia.com', 'dfa257255dc0335507b04c198dd1d765', 0x30, 0x31, 0x31, null, '19a3f6fca91ddf1e56664d9748208132', 0x30, 0x31, null),
        (634, 'Christoph', 'Oda', '1998-02-14', '5 Rowland Drive', '2510-515', 'Olho Marinho', 56482977, 29695210, 'coda1r@google.com.br', '45f5e8eece3fdce64623feea8b3a937c', 0x30, 0x30, 0x30, null, '8c689e86f11499c0754e4da248fdeddd', 0x30, 0x31, null),
        (635, 'Gorden', 'Kingwell', '1979-09-18', '6 Tomscot Terrace', '2411', 'Bago', 35105781, null, 'gkingwell1s@biblegateway.com', '275e00e37f1339d298d948b50cf7dae4', 0x30, 0x31, 0x31, null, 'adea7a243788210f38c9f262bbec59c8', 0x30, 0x31, null),
        (636, 'Shellysheldon', 'Harlett', '1965-01-28', '8 Esker Pass', '357637', 'Yessentuki', 72283251, 33161849, 'sharlett1t@devhub.com', 'f7b6e70b1d3340067f0b174f87378165', 0x31, 0x31, 0x31, null, 'cc8fbc8319cf5ab2dd5f3013e129666c', 0x30, 0x31, null),
        (637, 'Ritchie', 'Linkin', '1971-06-12', '549 Lakewood Center', '4242', 'Zudun', 21514378, 80399246, 'rlinkin1u@csmonitor.com', '50b8c7d6f05771beac25e52d934c2909', 0x31, 0x30, 0x31, null, '7625446001db127a9cf12803cc77545e', 0x30, 0x31, null),
        (638, 'Ely', 'Stanlock', '1989-11-30', '19 Dakota Plaza', '38740-000', 'Patrocínio', 58107308, 50792525, 'estanlock1v@360.cn', '2306c37d6ff35a4b185a74e62b2d2c5a', 0x31, 0x31, 0x31, null, '4db2a22dfc9b82999c0db5ddc102ac81', 0x30, 0x31, null),
        (639, 'William', 'Klaves', '1978-07-13', '234 Bunting Plaza', '90902', 'Dengyue', 94499453, null, 'wklaves1w@newsvine.com', '64436899877c9d6b8bf63319f61e53f5', 0x31, 0x30, 0x31, null, '1d0d4e2a81769e72468bbc0aed2b184e', 0x30, 0x31, null),
        (640, 'Nat', 'Pohling', '1984-03-11', '43411 New Castle Alley', '422', 'Ergates', 34347774, 93787655, 'npohling1x@t.co', 'bcc678866408897d8ca312e8f54d87ad', 0x30, 0x30, 0x31, null, '382356d2d34b71ab62255572787a03d5', 0x30, 0x31, null),
        (641, 'Jaymie', 'Piechnik', '1991-12-18', '12683 Park Meadow Trail', '429042', 'Higetegera', 34100318, null, 'jpiechnik1y@spiegel.de', '94d47887d0a3a7a6883415284c7616ed', 0x30, 0x30, 0x30, null, '5dfffb722cdae042c5f8d14e538543af', 0x30, 0x31, null),
        (642, 'Donovan', 'Eye', '1969-06-27', '120 Carberry Lane', '4242', 'Znomenka', 24242015, null, 'deye1z@phoca.cz', '305aa13358e39da97a71bcabca863fa6', 0x30, 0x31, 0x31, null, 'c5f7b2cd8567471eb9b00beae7feacc5', 0x30, 0x31, null),
        (643, 'Moshe', 'Savoury', '1975-08-16', '0 Homewood Terrace', '85295820', 'Koudougou', 17877556, null, 'msavoury20@msn.com', '035795f7e6432e1ab848348e856b2191', 0x30, 0x31, 0x31, null, '9c30d913788004533d1a7acc8a1da3df', 0x30, 0x31, null),
        (644, 'Bud', 'Kiloh', '1964-11-03', '487 8th Drive', 'JKIO034934', 'Mazra‘at Bayt Jinn', 35018647, 46103898, 'bkiloh21@seesaa.net', 'ea8c52b2986ffecf9827df89a6dd28a8', 0x30, 0x31, 0x30, null, '83e650e388f1f310e531fc41120c82cc', 0x30, 0x31, null),
        (645, 'Ewan', 'Muddiman', '1951-09-17', '6282 Spaight Way', '17011', 'Melchor de Mencos', 24594551, 48029141, 'emuddiman22@pen.io', 'c694842ee204b0713a8a78137407e45c', 0x30, 0x30, 0x30, null, 'd31872d1d5e4e426727218860f7d3978', 0x30, 0x31, null),
        (646, 'Franchot', 'Hissie', '1979-04-23', '686 Milwaukee Circle', '14900-000', 'Itápolis', 33459508, 64542558, 'fhissie23@1688.com', '0a7b1d5629b1f16589b89ec04c2ad41f', 0x30, 0x30, 0x31, null, 'ee0176ea7714e177492b18ac48338082', 0x30, 0x31, null),
        (647, 'Richie', 'Vasyukhnov', '1956-10-03', '15 Bunker Hill Road', '676776', 'Raychikhinsk', 13899861, null, 'rvasyukhnov24@sourceforge.net', '48b5b525242d3491b52be75829c42a78', 0x30, 0x30, 0x31, null, '3439ccccfc8c246dc436dbd0a5e7dc0a', 0x30, 0x31, null),
        (648, 'Stearn', 'Bradneck', '1976-07-27', '73377 Bartelt Junction', '3741', 'Skien', 91467664, 13737001, 'sbradneck25@wikimedia.org', 'fff5341311dd3266b891ce03d7bea52b', 0x30, 0x30, 0x30, 'visualize front-end markets', '7eadf16d84d3b28c678c56e637d85ff6', 0x30, 0x31, null),
        (649, 'Chas', 'Poore', '1971-11-17', '82025 International Place', 'US984829', 'Dingdian', 1469060, null, 'cpoore26@soundcloud.com', 'aac10495eca722f71e4c3a048495f910', 0x31, 0x30, 0x30, null, '890ab7dc97b98b7647ad0f73aef6695f', 0x30, 0x31, null),
        (650, 'Levy', 'Blanshard', '2000-11-19', '4 Butternut Plaza', '57770-000', 'Cajueiro', 11055131, null, 'lblanshard27@un.org', '5a059deff6bc99cee721cd46876b7716', 0x31, 0x30, 0x30, null, '321ebc1fe12e00fbd03e65ba4062f496', 0x30, 0x31, null),
        (651, 'Eward', 'Vittet', '1994-07-04', '47527 Kedzie Way', '8112', 'Santo Tomas', 42158402, 87561471, 'evittet28@yellowpages.com', '1c206e1efd779d2ef032262a44af3c2e', 0x31, 0x30, 0x30, 'deliver dynamic partnerships', 'ed9909f54b079f111c1d37061c5456b7', 0x30, 0x31, null),
        (652, 'Carter', 'Giannotti', '1950-09-15', '292 Granby Road', 'Peedd', 'Mitzpe Ramon', 99469555, null, 'cgiannotti29@washington.edu', '4fcf4763093af10c8a51929108c34bba', 0x30, 0x30, 0x31, null, '0d77aaf5bb80f2d48002b8ebef687227', 0x30, 0x31, null),
        (653, 'Yehudit', 'Westcarr', '1970-12-17', '90895 Bluestem Alley', '361366', 'Staryy Urukh', 72073477, null, 'ywestcarr2a@auda.org.au', '670b3616b2dc0bdab60636f66b3eeac9', 0x31, 0x30, 0x31, null, '70bb01a27bd7c5b54c45ff20978c922d', 0x30, 0x31, null),
        (654, 'Rikki', 'Mancktelow', '1962-05-19', '19920 Hooker Street', 'PW334', 'Almirante', 84899772, null, 'rmancktelow2b@sbwire.com', '664c1ab5a4731e071d95ddcd8d25e562', 0x30, 0x31, 0x30, null, 'c81de005a248c46bf5ff67943c210ad8', 0x30, 0x31, null),
        (655, 'Torr', 'Copes', '1964-06-05', '0342 Express Alley', '9430', 'Havana', 75152665, 68683560, 'tcopes2c@barnesandnoble.com', '0147983303665d1546d1393a2f456d3b', 0x31, 0x30, 0x31, null, '23e7ecbe90f2d67c04816181f097692a', 0x30, 0x31, null),
        (656, 'Duke', 'Coulton', '1988-07-13', '1641 Shasta Avenue', '87-603', 'Wielgie', 97477319, null, 'dcoulton2d@ow.ly', 'e23a03393f3d7886976a075859a3bc52', 0x31, 0x30, 0x31, 'productize holistic supply-chains', 'c6e985e27be12e85a66f0b46ec5be993', 0x30, 0x31, null),
        (657, 'Lenard', 'Pittford', '1979-02-19', '2085 Messerschmidt Plaza', '113 90', 'Stockholm', 6726071, null, 'lpittford2e@soup.io', 'b11e9c22339dc0d6587d692b81976d69', 0x30, 0x30, 0x31, null, 'c55c311ded47dae463f5014988f70d0f', 0x30, 0x31, null),
        (658, 'Ferd', 'Mival', '1957-08-19', '49502 Graceland Plaza', '433434', 'Huangjin', 39076424, 56035495, 'fmival2f@geocities.com', '8962e9afe5de4ecc3431b80d7a2d7c54', 0x30, 0x31, 0x30, null, '17590eb349d26eb67cc7caf33ceab9a0', 0x30, 0x31, null),
        (659, 'Orson', 'Cluatt', '1961-07-13', '558 Larry Center', '411067', 'Baraya', 8146138, null, 'ocluatt2g@feedburner.com', '550a1e9635e59cf4ef7d9021521da2c4', 0x31, 0x30, 0x30, null, 'a80ab10c6dee1f9cf46f6d23cbd9d828', 0x30, 0x31, null),
        (660, 'Baird', 'Maving', '2000-04-30', '2011 Bonner Park', '4334', 'Selouane', 48638396, 92488910, 'bmaving2h@scribd.com', 'd56ba84fe0e8a3968fa19d1fe47b2c01', 0x31, 0x30, 0x30, 'whiteboard enterprise deliverables', '51decc59f1de5f42e89e50626046d932', 0x30, 0x31, null),
        (661, 'Chauncey', 'Halloran', '1963-10-05', '184 Westend Alley', '14900-000', 'Thị Trấn Nghèn', 13039417, null, 'challoran2i@washington.edu', '3852366fffb975c8350b51f6acc15403', 0x30, 0x30, 0x30, null, '97cc2b67105bafc9474d4980cd160adb', 0x30, 0x31, null),
        (662, 'Jan', 'Rehn', '1981-03-16', '2 Stang Way', '414', 'Kolokani', 18007975, 61740915, 'jrehn2j@cdc.gov', 'c9914c04633471c12e196af16a5037f4', 0x30, 0x31, 0x30, null, 'b63ae50b4fa77e3c8153244fa15c5cb6', 0x30, 0x31, null),
        (663, 'Ricki', 'Treadaway', '1956-05-07', '4198 Hovde Pass', '141', 'Tejen', 94455870, 63338540, 'rtreadaway2k@zimbio.com', '434f4ea18cb4d9af66ee6e44b4e9bb9c', 0x31, 0x31, 0x30, null, '8ee1d2ef0ac68a88f47177adaf9582b8', 0x30, 0x31, null),
        (664, 'Ignatius', 'Kirkup', '1983-04-22', '252 3rd Road', '1141', 'Masaling', 45111901, 76981454, 'ikirkup2l@ebay.com', '60694dc0eacba2270a0d33f9b1b204eb', 0x31, 0x31, 0x31, null, '67e1c64eb82898ee87b1ad0abe91693e', 0x30, 0x31, null),
        (665, 'Bennie', 'Speers', '1971-07-08', '56 Mallory Hill', '3141', 'Hekou', 30073275, null, 'bspeers2m@google.co.uk', '379fba4b88361fbcaf240136395029bf', 0x31, 0x30, 0x30, null, 'c95db2735927eff203760f5fbb7b482f', 0x30, 0x31, null),
        (666, 'Efren', 'Gumme', '1970-10-28', '77541 Mosinee Hill', '943029', 'Lancai', 84654061, null, 'egumme2n@chicagotribune.com', '09c4fea4b391c6129b08dfddaeb350e4', 0x30, 0x31, 0x31, null, '60061c9be6119867b5e30d7c73b7e43d', 0x30, 0x31, null),
        (667, 'Darin', 'Gorthy', '1981-06-10', '73 Hazelcrest Alley', 'KD43443', 'Longtang', 84345644, null, 'dgorthy2o@timesonline.co.uk', '4981aa2d098db9cf4176b86795e07645', 0x31, 0x31, 0x30, null, '9c0ce3e775a7c3329c21501b8c22ffc9', 0x30, 0x31, null),
        (668, 'Giles', 'Mingauld', '1975-09-26', '7 Derek Trail', 'KLO090', 'Xiaojian', 5656744, null, 'gmingauld2p@mashable.com', '64a470474eb08b1d88170f2351e7a053', 0x30, 0x31, 0x31, 'architect interactive web-readiness', 'f3e6e293aba8de994d51145a77f95f9b', 0x30, 0x31, null),
        (669, 'Krisha', 'O'' Neligan', '1990-02-21', '6633 Ridgeview Parkway', '601525', 'Mezinovskiy', 57070607, null, 'koneligan2q@weibo.com', 'eba277783cb84067a405c25874e066b1', 0x30, 0x30, 0x30, null, '38a08480de5f374b8c185350550a5188', 0x30, 0x31, null),
        (670, 'Burton', 'Blaxton', '1964-10-26', '42 Mockingbird Alley', 'EW09090', 'Al Qarārah', 45603114, null, 'bblaxton2r@cisco.com', '305580d3fa8445f839dc9d1ab15a9ed4', 0x30, 0x30, 0x31, 'transform bricks-and-clicks markets', 'be50525fcb990ce5f96d5432aaf65066', 0x30, 0x31, null);

insert into reservation (idReservation, Apartment__FK, howManyGuests, dateFrom, dateTo, Price, Comments)
values  (1, 133, 4, '2019-06-16', '2019-07-23', 60565.4, ''),
        (2, 100, 2, '2019-06-08', '2019-06-24', 2763.37, null),
        (3, 125, 2, '2019-08-11', '2019-09-02', 17175.8, 'envisioneer 24/365 methodologies'),
        (4, 78, 3, '2019-06-27', '2019-07-05', 20160.6, null),
        (5, 9, 1, '2019-06-20', '2019-07-29', 48209.5, null),
        (6, 9, 2, '2019-05-15', '2019-05-29', 26331.1, null),
        (7, 78, 3, '2019-06-25', '2019-07-09', 95898.1, 'exploit end-to-end markets'),
        (8, 78, 2, '2019-07-09', '2019-08-27', 29343.5, null),
        (9, 135, 1, '2019-07-08', '2019-08-04', 96030.4, null),
        (10, 135, 3, '2019-05-30', '2019-07-25', 83214.5, null),
        (11, 100, 1, '2019-06-19', '2019-09-26', 26919.3, 'brand B2C web-readiness'),
        (12, 134, 2, '2019-07-09', '2019-08-02', 32430.4, null),
        (13, 26, 1, '2019-06-25', '2019-07-06', 55509.7, null),
        (14, 9, 3, '2019-06-22', '2019-10-03', 75337.8, null),
        (15, 26, 5, '2019-06-25', '2019-07-17', 21931.5, 'deploy sexy vortals'),
        (16, 134, 3, '2019-07-22', '2019-07-31', 5725.15, null),
        (17, 134, 4, '2019-10-20', '2019-11-13', 18109.9, null),
        (18, 134, 2, '2018-02-01', '2019-06-01', 38306.9, 'morph compelling functionalities'),
        (19, 148, 2, '2019-06-01', '2019-08-16', 19404.3, null),
        (20, 148, 4, '2019-07-11', '2019-09-10', 41103.9, null),
        (26, 45, 1, '2020-02-11', '2020-02-28', 70000, null),
        (27, 14, 1, '2020-03-01', '2020-03-16', 150000, null),
        (28, 3, 2, '2020-10-01', '2020-10-10', 2626.75, ''),
        (29, 72, 2, '2020-10-01', '2020-10-10', 2652.3, ''),
        (37, 102, 1, '2020-11-24', '2020-11-28', 3167.27, '');

insert into guest (guest_UserID__FK, Reservation__FK)
values  (528, 1),
        (531, 1),
        (530, 1),
        (17, 1),
        (668, 2),
        (662, 2),
        (651, 3),
        (651, 3),
        (16, 4),
        (13, 4),
        (645, 5),
        (597, 6),
        (601, 6),
        (601, 7),
        (597, 7),
        (641, 7),
        (622, 8),
        (623, 8),
        (600, 9),
        (587, 10),
        (563, 10),
        (601, 10),
        (11, 11),
        (12, 12),
        (13, 12),
        (645, 13),
        (587, 14),
        (600, 14),
        (608, 14),
        (1, 15),
        (625, 15),
        (587, 15),
        (569, 15),
        (2, 15),
        (540, 16),
        (603, 16),
        (8, 16),
        (600, 17),
        (601, 17),
        (602, 17),
        (603, 17),
        (570, 18),
        (575, 18),
        (576, 19),
        (601, 19),
        (600, 20),
        (604, 20),
        (625, 20),
        (647, 20);

insert into host (host_UserID__FK, idApartment__FK)
values  (11, 10),
        (11, 133),
        (11, 33),
        (11, 45),
        (11, 67),
        (11, 135),
        (11, 6),
        (11, 14),
        (11, 77),
        (11, 84),
        (568, 54),
        (568, 100),
        (568, 148),
        (531, 20),
        (531, 26),
        (531, 42),
        (531, 44),
        (550, 59),
        (550, 134),
        (550, 125),
        (531, 125),
        (550, 142),
        (12, 9),
        (529, 9),
        (538, 78),
        (529, 78),
        (12, 56),
        (538, 56),
        (529, 86),
        (12, 93),
        (529, 93),
        (538, 93),
        (14, 28),
        (14, 38),
        (569, 83),
        (569, 149),
        (8, 3),
        (8, 121),
        (5, 121),
        (559, 3),
        (13, 15),
        (534, 18),
        (534, 30),
        (534, 110),
        (534, 116),
        (4, 24),
        (17, 24),
        (561, 24),
        (544, 55),
        (551, 29),
        (543, 71),
        (565, 36),
        (1, 16),
        (1, 81),
        (1, 109),
        (532, 151),
        (1, 152),
        (1, 153);

insert into review (idReview, Title, Description, Rate, Date, Reservation__FK, Apartment__FK, Guest__FK, howLong_days)
values  (21, 'Wszystko ok', 'Wszystko ok', 5, '2020-12-14', 1, 133, 528, 4),
        (22, 'Git', 'Git', 4, '2019-12-14', 7, 78, 641, 20),
        (23, 'Brud', 'Brud w łazience', 3, '2019-08-14', 7, 78, 601, 14),
        (24, 'OK', 'Dla mnie spoko', 5, '2020-08-19', 7, 78, 597, 41),
        (26, 'Złodzieje!', 'Nie mam portfela po pobycie tutajh', 1, '2020-12-19', 12, 134, 13, 24),
        (27, 'Super', 'Nic nie ginie', 4, '2020-12-14', 16, 134, 540, 3),
        (28, 'Ekstra', 'Uczciwi gospodarze', 4, '2020-12-16', 16, 134, 603, 5),
        (29, 'Dobrze', 'Odpoczalem', 5, '2020-12-29', 16, 134, 8, 4),
        (30, 'Dobrze', 'Jak wyzej', 4, '2020-11-14', 20, 148, 600, 6),
        (31, 'OK', 'Dobrze', 5, '2020-11-18', 20, 148, 604, 7),
        (32, 'OK', 'Troche za twarde materace', 4, '2020-11-14', 20, 148, 625, 10),
        (33, 'Wszystko dobrze', 'Ja nie narzekam', 5, '2020-12-14', 20, 148, 647, 24);

		
#funkcja sumujaca dochody gospodarza (host) z wszystkich rezerwacji jego mieszkan

drop function if exists hostSumOfReservation;

delimiter //

create function hostSumOfReservation (id_host int)
	returns float

begin

    declare summary float default 0.00;
    select SUM(Price) from host
    join apartment a on host.idApartment__FK = a.idApartment
    join reservation r on a.idApartment = r.Apartment__FK
    where host_UserID__FK = id_host into summary;


    return summary;
end // 

delimiter ;

#dodaje uzytkownika

drop procedure if exists addUser;
DELIMITER //
create
    procedure addUser(IN name varchar(127), IN surname varchar(127), IN birthdate date,
                                               IN address varchar(255), IN postcode varchar(15), IN city varchar(64),
                                               IN primaryphone int, IN secondaryphone int, IN email varchar(255),
                                               IN password varchar(255), IN is_active binary(1),
                                               IN is_confirmed binary(1), IN is_banned binary(1),
                                               IN comments varchar(8192), IN payment_method varchar(255),
                                               IN ishost binary(1), IN isguest binary(1), IN localization int)
begin
    INSERT INTO user(Name, Surname, BirthDate, Address, PostCode, City, PrimaryPhoneNumber, SecondaryPhoneNumber, Email, Password, isActive, isConfirmed, isBanned, Comments, idPaymentMethod, isHost, isGuest, UserLocalization_FK) values
    (name, surname, birthdate, address, postcode, city, primaryphonenumber, secondaryphonenumber, email, md5(password), isactive, isconfirmed, isbanned, comments, md5(idpaymentmethod), ishost, isguest, userlocalization_fk);
end //

DELIMITER ;

#dodawanie hosta do mieszkania poza dodaniem mieszkania, przydatne przy dodawaniu wspolnika

drop procedure if exists addHost;
DELIMITER //


create
   procedure addHost(IN id_host int)
begin
    insert into host(host_UserID__FK, idApartment__FK) values (id_host, last_insert_id());
end //

DELIMITER ;

#dodaje typ mieszkania

drop procedure if exists addApartType;
DELIMITER //

create
   procedure addApartType(IN type varchar(127), IN class char(16),
                                                    IN description varchar(4095))
begin
    INSERT INTO apartmenttype(TypeName, Class, Description) values (type, class, description);
end //

delimiter ;
#dodaje apartment i jego gospodarza

drop procedure if exists addApartment;


delimiter //

create procedure addApartment(IN aparttype int, IN localization int, IN addrress varchar(255),
                                                    IN apartnumber int, IN title varchar(255), IN descr varchar(8192),
                                                    IN equip varchar(2047), IN area float, IN rooms tinyint,
                                                    IN is_longterm binary(1), IN is_availaible binary(1),
                                                    IN one_night_price float, IN discount float,
                                                    IN comments varchar(4096), in id_host int)
begin


    INSERT INTO apartment(ApartmentType__FK, ApartmentLocalization__FK, Address, ApartNumber, Title, Description, Equipment, Area, Rooms, isLongTerm, isAvailable, oneNightPrice, Discount, Comments)
    values (aparttype, localization, addrress, apartnumber, title, descr, equip, area, rooms, is_longterm, is_availaible, one_night_price, discount, comments);

    INSERT INTO host(host_UserID__FK, idApartment__FK) values (id_host,last_insert_id());

end //
delimiter ;

drop function if exists isHostActionsAllowed;
DELIMITER //

create
     function isHostActionsAllowed(apart_id int) returns binary(1)
begin

    declare permission_temp binary(1);
    declare ban binary(1);
    declare active binary(1);
    declare done int;
    declare permission binary(1) default 1;

    declare read_hosts cursor for select isActive, isBanned from apartment
    join host h2 on apartment.idApartment = h2.idApartment__FK
    join user u2 on h2.host_UserID__FK = u2.idUser where idApartment = apart_id;

   declare continue handler for not found set done = 1;

    OPEN read_hosts;

    calc_perm: loop
        fetch read_hosts into active, ban;

        if done then
            leave calc_perm;
        end if;

        if ((ban = 0) and (active =1))
            then set permission_temp = 1;
        else
            set permission_temp = 0;
        end if;
        
        set permission = ((permission_temp) and (permission));
        
    end loop calc_perm;

    ClOSE read_hosts;
    
    return permission;
end //

DELIMITER ;


drop function if exists canUserDoIt;
DELIMITER //

create
    function canUserDoIt(user_id int) returns binary(1)
begin

    declare permission_temp binary(1);
    declare ban binary(1);
    declare active binary(1);
    declare done int;
    declare permission binary(1) default 1;

   declare read_user cursor for select isActive, isBanned from user where idUser = user_id;

   declare continue handler for not found set done = 1;

    OPEN read_user;

    calc_perm: loop
        fetch read_user into active, ban;

        if done then
            leave calc_perm;
        end if;

        if ((ban = 0) and (active =1))
            then set permission_temp = 1;
        else
            set permission_temp = 0;
        end if;
        
        set permission = ((permission_temp) and (permission));
        
    end loop calc_perm;

    ClOSE read_user;
    
    return permission;

end //

DELIMITER;


drop function if exists calcPrice;
DELIMITER //

create
   function calcPrice(id_apart int, date_from date, date_to date) returns float
begin

    declare one_night_price float;
    declare summary float;
    declare discount_val float;
    declare nights_value int;

    select oneNightPrice from apartment where (idApartment = id_apart) into one_night_price;
    select Discount from apartment where (idApartment = id_apart) into discount_val;

    set nights_value = datediff(date_to, date_from);

    if (discount_val = 0)
        then set summary = nights_value * one_night_price;
    else
        set summary = (nights_value * one_night_price)*(1.00-discount_val);
    end if;

    return summary;
end //

DELIMITER ;

drop procedure if exists makeReservation;
DELIMITER //
create
     procedure makeReservation(IN user_id int, IN apart int, IN howmanyguest int,
                                                       IN datefrom datetime, IN dateto datetime,
                                                       IN comment varchar(4096))
begin
    declare isAllowed binary(1);
    declare isHostAllowed binary(1);

    declare price float;
    declare permission binary(1);

    select canUserDoIt(user_id) into isAllowed;
    select isHostActionsAllowed(apart) into isHostAllowed;


        if ((isHostAllowed = 1) and (isAllowed = 1))
        then set permission = 1;
        else
            set permission = 0;
    end if;

    process:begin
		if (permission = 0)
			then select 'Permission denied!';
			leave process;
        else

            select calcPrice(apart, datefrom, dateto) into price;
          
            INSERT INTO reservation(apartment__fk, howmanyguests, datefrom, dateto, price, comments) VALUES (apart, howmanyguest, datefrom, dateto, price, comment);
            insert into guest(guest_UserID__FK, Reservation__FK) values (user_id, last_insert_id());
        end if;
        end process;
end //

DELIMITER ;

drop procedure if exists inviteFriends;
DELIMITER //

create
    procedure inviteFriends(IN reservation_id int, IN id_guest int)
begin

    declare isAllowed binary(1);
    select canUserDoIt(id_guest) into isAllowed;

    proc:begin
        if (isAllowed = 0)
            then leave proc;
        else
            insert into guest (guest_UserID__FK, Reservation__FK) values (id_guest, reservation_id);
        end if;
    end proc;
end //

DELIMITER ;


drop procedure if EXISTS createReview;
DELIMITER //


create
    procedure createReview(IN guestID int, IN reservation int, IN title_rev varchar(512),
                                                    IN descr varchar(8192), IN user_rate int)
begin

    declare days int;
    declare date_from date;
    declare date_to date;
    declare current_datetime date;
    declare apart int;
    declare prev_rate float;
    declare new_rate float;

    select dateFrom, dateTo, idApartment, Rate from guest
    join reservation r on r.idReservation = guest.Reservation__FK
    join apartment a on r.Apartment__FK = a.idApartment
    where (Reservation__FK = reservation) and (guest_UserID__FK = guestID)
    into date_from, date_to, apart, prev_rate;

    set days = datediff(date_to, date_from);

    set current_datetime = current_date;

    if (prev_rate is null )
        then set new_rate = user_rate;
     elseif (prev_rate >= 0)
        then set new_rate = ((prev_rate+user_rate)/2);
    end if;
   
    UPDATE apartment SET Rate = new_rate where idApartment=apart;
    INSERT INTO review(Title, Description, Rate, Date, Reservation__FK, Apartment__FK, Guest__FK, howLong_days) values (title_rev, descr, user_rate, current_datetime, reservation, apart, guestID, days);
end //

DELIMITER ;


drop procedure if exists showHistoryTravel;
DELIMITER //

create
    procedure showHistoryTravel(IN user_id int)
begin
    select user.Name, user.Surname, l.City, c.Name as Country, c.Continent from user 
        join guest g on user.idUser = g.guest_UserID__FK
        join reservation r on g.Reservation__FK = r.idReservation
        join apartment a on r.Apartment__FK = a.idApartment
        join localization l on a.ApartmentLocalization__FK = l.idLocalization
        join country c on l.idCountry_FK = c.idCountry

    where idUser = user_id;
end //

DELIMITER ;

drop procedure if exists showHostContactData;
DELIMITER //


create
  procedure showHostContactData(IN host_id int)
begin

      declare temp_phonenumber int;
    declare temp_prefix int;
   declare phonenumber char(24);
    declare prefix char(10);
    declare is_host binary(1);
    
    select PrimaryPhoneNumber, Prefix, isHost from user
    join localization l on UserLocalization_FK = l.idLocalization
    join country c on l.idCountry_FK = c.idCountry where idUser = host_id
    into temp_phonenumber, temp_prefix, is_host;
    set phonenumber = cast(temp_phonenumber as CHAR(24));
    set prefix = cast(temp_phonenumber as CHAR(10));
    set phonenumber = concat('+',prefix,phonenumber);

    proces:begin 
        if (is_host = 0)
			then select 'Host does not exists!';
            leave proces;
        else
                select user.Name, Surname, Address, user.City, PostCode, phonenumber, Email, isActive, isBanned, l2.City as CurrentLocalization, c2.Name as Country from user
                 join localization l2 on UserLocalization_FK = l2.idLocalization
                    join country c2 on l2.idCountry_FK = c2.idCountry
                 where idUser = host_id;
        end if;
    end proces;
    


end //

DELIMITER ;

drop procedure if exists showSumHost;
DELIMITER //

create
    procedure showSumHost(IN host int)
begin
    select Name, Surname, hostSumOfReservation(host)
    from user
    where idUser = host;
end //

DELIMITER ;
