create table IF NOT EXISTS apartmenttype
(
    idApartType int auto_increment,
    TypeName    varchar(127) charset utf8mb4  not null,
    Class       char(16) charset utf8mb4      not null,
    Description varchar(4095) charset utf8mb4 null,
    constraint ApartmentType_idApartType_uindex
        unique (idApartType)
)
    comment 'Klasy, rodzaje mieszkań';

alter table apartmenttype
    add primary key (idApartType);

create table IF NOT EXISTS country
(
    idCountry tinyint auto_increment,
    Name      varchar(128) not null,
    Code      varchar(8)   not null,
    Prefix    int          not null,
    Continent varchar(128) not null,
    constraint Country_idCountry_uindex
        unique (idCountry)
)
    comment 'Panstwa' charset = utf8mb4;

alter table country
    add primary key (idCountry);

create table IF NOT EXISTS localization
(
    idLocalization int auto_increment,
    City           varchar(255) charset utf8mb4 not null,
    idCountry_FK   tinyint                      not null,
    constraint localization_idLocalization_uindex
        unique (idLocalization),
    constraint country___fk
        foreign key (idCountry_FK) references country (idCountry)
)
    comment 'Miasta, miejscowości';

alter table localization
    add primary key (idLocalization);



create table IF NOT EXISTS apartment
(
    idApartment               int auto_increment,
    ApartmentType__FK         int           not null,
    ApartmentLocalization__FK int           not null,
    Address                   varchar(255)  not null,
    ApartNumber               int           null,
    Title                     varchar(255)  not null,
    Description               varchar(8192) null,
    Equipment                 varchar(2047) not null,
    Area                      float         not null,
    Rooms                     tinyint       not null,
    isLongTerm                binary(1)     not null,
    isAvailable               binary(1)     not null,
    oneNightPrice             float         not null,
    Discount                  float         null,
    Comments                  varchar(4096) null,
    Rate                      float         null,
    constraint Apartment_idApartment_uindex
        unique (idApartment),
    constraint ApartmentLocalization__FK
        foreign key (ApartmentLocalization__FK) references localization (idLocalization),
    constraint ApartmentType__FK
        foreign key (ApartmentType__FK) references apartmenttype (idApartType)
)
    comment 'Apartamenty, pokoje, mieszkania, etc.' charset = utf8mb4;

alter table apartment
    add primary key (idApartment);
	
create table IF NOT EXISTS user
(
    idUser               int auto_increment,
    Name                 varchar(127)  not null,
    Surname              varchar(127)  not null,
    BirthDate            date          not null,
    Address              varchar(255)  not null,
    PostCode             varchar(15)   not null,
    City                 varchar(64)   not null,
    PrimaryPhoneNumber   bigint        not null,
    SecondaryPhoneNumber int           null,
    Email                varchar(255)  not null,
    Password             varchar(255)  not null,
    isActive             binary(1)     not null,
    isConfirmed          binary(1)     not null,
    isBanned             binary(1)     not null,
    Comments             varchar(8192) null,
    idPaymentMethod      varchar(255)  not null,
    isHost               binary(1)     not null,
    isGuest              binary(1)     not null,
    UserLocalization_FK  int           null,
    constraint User_idUser_uindex
        unique (idUser),
    constraint user_Email_uindex
        unique (Email),
    constraint user_idPaymentMethod_uindex
        unique (idPaymentMethod),
    constraint UserLocalization___fk
        foreign key (UserLocalization_FK) references localization (idLocalization)
            on update set null on delete set null
)
    charset = utf8mb4;

alter table user
    add primary key (idUser);
	
create table IF NOT EXISTS reservation
(
    idReservation    int auto_increment,
    Apartment__FK    int           not null,
    howManyGuests    int           not null,
    dateFrom         datetime      not null,
    dateTo           datetime      not null,
    Price            float         not null,
    Comments         varchar(4096) null,
    constraint Reservation_idReservation_uindex
        unique (idReservation),
    constraint Apartment_Reservation___FK
        foreign key (Apartment__FK) references apartment (idApartment)
            on update cascade on delete cascade
)
    comment 'Rezerwacje' charset = utf8mb4;

alter table reservation
    add primary key (idReservation);

create table IF NOT EXISTS host
(
    host_UserID__FK int not null,
    idApartment__FK int not null,
    constraint Apartment___fk
        foreign key (idApartment__FK) references apartment (idApartment)
            on update cascade on delete cascade,
    constraint host_user_idUser__FK
        foreign key (host_UserID__FK) references user (idUser)
            on update cascade on delete cascade
)
    charset = utf8mb4;

create table IF NOT EXISTS guest
(
    guest_UserID__FK int not null,
    Reservation__FK  int null,
    constraint Guest__FK
        foreign key (guest_UserID__FK) references user (idUser)
            on update cascade on delete cascade,
    constraint Reservation___FK
        foreign key (Reservation__FK) references reservation (idReservation)
)
    charset = utf8mb4;

create table review
(
    idReview        int auto_increment,
    Title           varchar(512)  not null,
    Description     varchar(8192) not null,
    Rate            tinyint       not null,
    Date            date          not null,
    Reservation__FK int           not null,
    Apartment__FK   int           not null,
    Guest__FK       int           not null,
    howLong_days    int           not null,
    constraint review_idReview_uindex
        unique (idReview),
    constraint Apartment__FK
        foreign key (Apartment__FK) references apartment (idApartment)
            on update cascade on delete cascade,
    constraint Guest___FK
        foreign key (Guest__FK) references guest (guest_UserID__FK)
            on update cascade on delete cascade,
    constraint ReservationReview__FK
        foreign key (Reservation__FK) references guest (Reservation__FK)
            on update cascade on delete cascade
)
    comment 'recenzje' charset = utf8mb4;

alter table review
    add primary key (idReview);